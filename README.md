# Countering UAlbertaBot #

## What is this? ##

* This repository contains a StarCraft bot written in C++ based on [UAlberta bot](https://github.com/davechurchill/ualbertabot/wiki)
* It implements Bayesian reasoning from [Opening tech](https://github.com/syhw/OpeningTech)
* It counters recognized openings by our custom strategies
* It implements advanced scouting module using the Bayesian network to determine ideal scout timing
* It also uses our map analysis algorithm to find a safe path to scout

This bot was implemented as part of my bachelor thesis [Integrating Probabilistic Model for Detecting Opponent Strategies Into a Starcraft Bot](https://is.cuni.cz/webapps/zzp/detail/158726/32363086). 
All details about the implementation, algorithms and concepts can be found in the thesis in directory **Thesis** of the repository.

## Abstract

Recent research in artificial intelligence (AI) for real time strategies (RTS) has shown a great need for
a computer controlled agent (bot) to be able to adapt its strategy in response to opponent’s actions.
While some progress has been made in detecting opponent’s strategies offline, there has not been
much success in using this information to guide in-game decisions. We present a version of
UAlbertaBot enhanced by existing probabilistic algorithm for supervised learning from replays and
strategy prediction. Bot that adapts its strategies has proved to be superior to a random bot as we
show in simulated StarCraft: Brood War AI tournament. Our work exposes the importance of
scouting and strategy adaptation. By further improvement of strategies, a bot capable of competing
with human players may be created.


## How to get set up ##

The setup is similar to the setup of UAlberta bot.

### Building the bot ###

The repository contains Visual studio 2013 project. However, **msvc90** (VS 2008) is needed to compile the project.

1. Install StarCraft: Brood War and patch to the latest version (1.16.1)
1. Install Visual Studio 2008 (for the msvc90 compiler)
1. Install Visual Studio 2013 (to open the project)
1. Install [BWAPI 3](https://github.com/bwapi/bwapi/releases/tag/v3.7.5). Installation instructions are in README file of the downloaded BWAPI zip file.
1. Install [BOOST](http://www.boost.org/) libraries version 1.50 or greater for **msvc90 32-bit**.
1. Create system environment variable **SC_DIR** which points to the directory where you installed StarCraft (where the StarCraft.exe is)
1. Create system environment variable **BOOST_DIR** which points to the directory you installed BOOST into.
1. Create system environment variable **BWAPI_DIR** which points to the directory you installed BWAPI into.
1. Open the project **UAlbertaBot.sln** in Visual Studio 2013.
    * If asked to upgrade to newer version of the compiler click Cancel
    * Open UAlbertaBot project settings (right click and properties) and in the platform toolset, check that Visual Studio 2008 (v90) is selected with no issues.
    * If VS says it cannot detect the v90 toolset (even if you installed VS 2008) you will need to install Visual C++ 2010 express (for some reason).
    * After that reopen the project and all should be fine.
1. Build the project in Release Win32 configuration
1. If you set up the SC_DIR variable correctly the UAlbertaBot.dll has been copied into your SC_DIR/bwapi-data/AI and is ready to run.
1. Copy the **"tables"** directory containing the dataset to the folder where the UAlbertaBot.dll is.

#### Troubleshooting ####
* If the automatic installation of BWAPI does not work for some reason simply copy the files as described in the BWAPI README.
* If you do not want to build BOOST yourself just Google Boost Pre-Built Binaries.
* If you do not want to create the system variables or you want different names for them you will need to change the project settings accordingly.
* If the build fails make sure you have the correct environment variables. Also make sure that your BOOST lib files are in **BOOST_DIR/stage/lib**.

### Running the bot ###

Running the bot works same as running any other BWAPI c++ bot using Chaoslauncher.

1. Run Chaoslauncher.exe from **BWAPI_DIR/Chaoslauncher**
1. In the Settings tab fill the correct path to your StarCraft installation and hit OK
1. In the plugins tab make sure only **BWAPI Injector (1.16.1) RELEASE** and **W-MODE 1.02** are checked (IICUP may be selected as well)
1. While having selected the BWAPI Injector (1.16.1) hit the Config button and a text config file will open
1. In the config file set **ai = bwapi-data/AI/UAlbertaBot.dll** and save it
1. Click the Start button to run StarCraft in window mode
1. Navigate to Single Player - Expansion - Your name - Play Custom - Select any map - Next to your name select Protoss and hit OK
    * The game will start and the bot will play the whole game
    * The game may be unresponsive for some time (even more than a minute). The bot performs terrain analysis during this time and loads the dataset.
    * The second time you play the same map the analysis should be already cached and the wait time should be much lower

#### Troubleshooting ####
If the bot crashes at the beginning of the match make sure you have the "tables" directory with the dataset tables (PvP.table etc.) in the same directory as UAlbertaBot.dll

More details about how to run BWAPI bots can be found at the [BWAPI website](bwapi.github.io) or at the [UAlberta bot]() website.


### Disclaimer ###
There are a lot of unfinished things in the bot (of course). It is more of a baseline for future work than a finished universal bot. Feel free to dig around.

### Who do I talk to? ###
If you have any questions you can send me an email.