#pragma once

#include "Common.h"
#include "BWTA.h"
#include "base/BuildOrderQueue.h"
#include "InformationManager.h"
#include "base/WorkerManager.h"
#include "base/StarcraftBuildOrderSearchManager.h"
#include <sys/stat.h>
#include <cstdlib>
#include "PredictionManager.h"
#include <fstream>

#include "..\..\StarcraftBuildOrderSearch\Source\starcraftsearch\StarcraftData.hpp"

typedef std::pair<int, int> IntPair;
typedef std::pair<MetaType, UnitCountType> MetaPair;
typedef std::vector<MetaPair> MetaPairVector;

class StrategyManager 
{
public:
	/** All supported strategies in no particular order.
	*	Our bot plays only Protoss. */
	enum PStrategies {
		ProtossAir = 0,
		ProtossAntiAir,
		ProtossRobo,
		ProtossHT,
		Protoss4Gate,
#if ORIGINAL_BO
		ProtossZealotRush,
		ProtossDarkTemplar,
		ProtossDragoons,
#endif
		NumProtossStrategies
	};

private:
	StrategyManager();
	~StrategyManager();

	std::vector<std::string>	protossOpeningBook;
	std::string					stdOpening;

	std::string					readDir;
	std::string					writeDir;
	std::vector<IntPair>		results;
	std::vector<int>			usableStrategies;
	int							currentStrategy;

	BWAPI::Race					selfRace;
	BWAPI::Race					enemyRace;

	bool						firstAttackSent;
	bool						hasFinalStrategy;
	bool firstTimeInvis;

	void	addStrategies();
	void	setStrategy();
	void	readResults();
	void	writeResults();

	const	int					getScore(BWAPI::Player * player) const;
	const	double				getUCBValue(const size_t & strategy) const;
	
#if ORIGINAL_BO
	// tmp
	const	MetaPairVector		getProtossTmpStratBuildOrderGoal() const;
	
	// protoss strategy
	const	bool				expandProtossZealotRush() const;
	const	std::string			getProtossZealotRushOpeningBook() const;
	const	MetaPairVector		getProtossZealotRushBuildOrderGoal() const;

	const	bool				expandProtossDarkTemplar() const;
	const	std::string			getProtossDarkTemplarOpeningBook() const;
	const	MetaPairVector		getProtossDarkTemplarBuildOrderGoal() const;

	const	bool				expandProtossDragoons() const;
	const	std::string			getProtossDragoonsOpeningBook() const;
	const	MetaPairVector		getProtossDragoonsBuildOrderGoal() const;

	const	MetaPairVector		getTerranBuildOrderGoal() const;
	const	MetaPairVector		getZergBuildOrderGoal() const;
#endif
	const	MetaPairVector		getProtossOpeningBook() const;
	const	MetaPairVector		getTerranOpeningBook() const;
	const	MetaPairVector		getZergOpeningBook() const;

	// Common parts of strategies
	/** Handles anti-invis for all strategies */
	void				HandleInvis(MetaPairVector &goal);
	/** Handles scouting unit production for all strategies */
	void				HandleScouting(MetaPairVector &goal) const;

	// New build orders compatible with prediction manager
	const	MetaPairVector		getProtossAir();
	const	MetaPairVector		getProtossAntiAir();
	const	MetaPairVector		getProtossRobo();
	const	MetaPairVector		getProtossHT();
	const	MetaPairVector		getProtoss4Gate();

	/** Helper functions to add given research to the goal if not already researched */
	const	bool				tryResearch(MetaPairVector &goal, const BWAPI::UpgradeType &upgrade) const;
	const	bool				tryResearch(MetaPairVector &goal, const BWAPI::TechType &tech) const;

	/** If prediction manager used we set best strategy based on prediction */
	void				setBestStrategy();

	/** Given probability distribution selects best counter strategy for it */
	const	PStrategies			getProtossCounter(const std::vector<OpeningProbaPair>& probas);
	const	PStrategies			getTerranCounter(const std::vector<OpeningProbaPair>& probas);
	const	PStrategies			getZergCounter(const std::vector<OpeningProbaPair>& probas);

			std::string			getHighestProbaStrat(const std::vector<OpeningProbaPair>& probas) const;

	// Scouting
	/** The shortest interval between two scouts can be sent */
	const int NEW_SCOUT_TIME_THOLD;
	
	/** The smallest probability we will send scout to see if enemy OP has changed */
	const double OP_CHANGE_PROBA_THOLD;
	
	/** When can we send advanced scout after the game started? */
	const int EARLIEST_ADV_SCOUT_TIME;

	/** When should we force build flying unit to scout */
	const int BUILD_FLY_SCOUT_TIME;

	/** When should we add units to our current strategy (just add more, no new) */
	const int ADD_UNITS_TO_STRAT;
	
	/** When is the last time we can send ground scout (for it to be effective and see enemy base) */
	const int GROUND_SCOUT_TIME_THOLD;

	/** Return current strategy as a string */
	const	std::string			getStrategyName(int strategy) const;
public:
	static	StrategyManager &	Instance();

			void				onStart();
			void				onEnd(const bool isWinner);
	
	
	const	bool				doAttack(const std::set<BWAPI::Unit *> & freeUnits);
	const	int				    defendWithWorkers();

	const	int					getCurrentStrategy() const;
	const	std::string			getCurrentStrategyName() const;
	

	const	MetaPairVector		getBuildOrderGoal(bool finalStrat = false);
	const	std::string			getOpeningBook() const;

	/** Returns whether is our current strategy final */
	bool isStratFinal() const { return hasFinalStrategy; }

	// Scouting
	/** Returns whether we should scout or not with given unit from strategic point of view */
	bool ShouldScout(const BWAPI::Unit* scout) const;
};
