#include "PredictionManager.h"
#include "enums_name_tables_tt.h"
#include "Logger.h"
#include "base/ProductionManager.h"
#include <fstream>
#include <iostream>

using namespace BWAPI;

void PredictionManager::getLearnedBuildings()
{
	Race enemyRace = Broodwar->enemy()->getRace();
	std::ofstream s;
	std::ofstream known_s;
	std::string output = "c:\\_SC_Log\\units_" + enemyRace.getName() + ".txt";
	std::string knownOutput = "c:\\_SC_Log\\units_known_" + enemyRace.getName() + ".txt";
	s.open(output.c_str(), std::ofstream::out);
	known_s.open(knownOutput.c_str(), std::ofstream::out);
	
	for (size_t i = 0; i < serializedTables.vector_X.size(); i++)
	{
		for (std::set<int>::const_iterator j = serializedTables.vector_X[i].begin(); j != serializedTables.vector_X[i].end(); ++j)
		{
			learnedBuildings.insert(*j);
		}
	}

	missingUnitTypes.clear();
	for (size_t i = 0; i < 50; ++i)
	{
		if (learnedBuildings.find(i) != learnedBuildings.end())
		{
			s << i << std::endl;
		}
		else
		{
			s << i << " missing" << std::endl;
			missingUnitTypes.insert(i);
		}
	}
	s.close();

	for (std::set<int>::const_iterator it = learnedBuildings.begin(); it != learnedBuildings.end(); ++it)
	{
		known_s << *it << std::endl;
	}
	known_s.close();
}

void PredictionManager::logAllBuildTrees()
{
	Race enemyRace = Broodwar->enemy()->getRace();
	std::ofstream s;
	std::string output = "c:\\_SC_Log\\bts_" + enemyRace.getName() + ".txt";
	s.open(output.c_str(), std::ofstream::out);

	for (size_t i = 0; i < serializedTables.vector_X.size(); i++)
	{
		for (std::set<int>::const_iterator j = serializedTables.vector_X[i].begin(); j != serializedTables.vector_X[i].end(); ++j)
		{
			s << *j << " ";
		}
		s << std::endl;
	}
	s.close();
}

void PredictionManager::LoadTable(const std::string& tablePath)
{
	std::ifstream input(tablePath.c_str());
	boost::archive::text_iarchive iArchive(input);

	iArchive >> serializedTables;
	tableLoaded = true;

	/*getLearnedBuildings();
	logAllBuildTrees();*/
}

void PredictionManager::InitializePrior(const std::string& filename, const Race& race, const bool random)
{
	// Setup for given race - Protoss is default
	openings_knowing_player * op_prior_ref = &op_prior;
	serialized_tables * serializedTables_ref = &serializedTables;

	// If we don't know the race we want to populate all containers
	// If we know the race, we already only populated the right containers (serializedTables)
	// and others - TvP are empty
	if (random)
	{
		if (race == Races::Terran)
		{
			op_prior_ref = &op_prior_TvP;
			serializedTables_ref = &TvP;
		}
		else if (race == Races::Zerg)
		{
			op_prior_ref = &op_prior_ZvP;
			serializedTables_ref = &ZvP;
		}
	}

	// uniform prior
	size_t nbOpenings = serializedTables_ref->openings.size();
	op_prior_ref->numberGames = std::vector<int>(LEARNED_TIME_LIMIT / 60, 1);
	for (size_t t = 0; t < LEARNED_TIME_LIMIT / 60; ++t)
		op_prior_ref->tabulated_P_Op_knowing_Time.push_back(std::vector<long double>(nbOpenings, 1.0 / nbOpenings));
}

PredictionManager::PredictionManager() 
	: tableLoaded(false), notFirstOverlord(false)
{
	/// Load the learned prob tables (uniforms + bell shapes) for the right match up
	/// This is not obfuscation, the learning of these tables is described in the CIG 2011 Paper
	/// A Bayesian Model for Opening Prediction in RTS Games with Application to StarCraft, Gabriel Synnaeve, Pierre Bessiere, CIG (IEEE) 2011
	/// All code for the learning is here: https://github.com/SnippyHolloW/OpeningTech
	{
		Race enemyRace = Broodwar->enemy()->getRace();
		std::string serializedTablesFileName("bwapi-data/AI/tables/");

		if (enemyRace == Races::Protoss || enemyRace.getName() == "Protoss")
		{
#if GABRIEL_LABELS
			serializedTablesFileName.append("PvPx.table");
#else
			serializedTablesFileName.append("PvP.table");
#endif
			LoadTable(serializedTablesFileName);
		}
		else if (enemyRace == Races::Terran || enemyRace.getName() == "Terran")
		{
#if GABRIEL_LABELS
			serializedTablesFileName.append("TvPx.table");
#else
			serializedTablesFileName.append("TvP.table");
#endif
			LoadTable(serializedTablesFileName);
		}
		else if (enemyRace == Races::Zerg || enemyRace.getName() == "Zerg")
		{
#if GABRIEL_LABELS
			serializedTablesFileName.append("ZvPx.table");
#else
			serializedTablesFileName.append("TvP.table");
#endif
			LoadTable(serializedTablesFileName);
		}
		else // Random
		{
			std::string tmpTvP(serializedTablesFileName);
			std::string tmpZvP(serializedTablesFileName);

#if GABRIEL_LABELS
			serializedTablesFileName.append("PvPx.table");
			tmpTvP.append("TvPx.table");
			tmpZvP.append("ZvPx.table");
#else
			serializedTablesFileName.append("PvP.table");
			tmpTvP.append("TvP.table");
			tmpZvP.append("ZvP.table");
#endif

			std::ifstream ifs(serializedTablesFileName.c_str());
			boost::archive::text_iarchive ia(ifs);
			ia >> serializedTables;

			std::ifstream ifs2(tmpTvP.c_str());
			boost::archive::text_iarchive ia2(ifs2);
			ia2 >> TvP;

			std::ifstream ifs3(tmpZvP.c_str());
			boost::archive::text_iarchive ia3(ifs3);
			ia3 >> ZvP;
		}
	}

	/// Initialize openingsProbas with a uniform distribution
	size_t nbOpenings = serializedTables.openings.size();
	openingsProbas = std::vector<long double>(nbOpenings, 1.0 / nbOpenings);

	/// Initialize op_priors from what we saw of the opponent
	std::string filename("bwapi-data/read/xop_prior_");
	
	filename.append(Broodwar->enemy()->getName());
	filename.append("_");
	Race enemyRace = Broodwar->enemy()->getRace();

	// For Random
	std::string filename_TvP = filename;
	std::string filename_ZvP = filename;
	if (Broodwar->enemy()->getRace() != Races::Protoss
		&& Broodwar->enemy()->getRace() != Races::Terran
		&& Broodwar->enemy()->getRace() != Races::Zerg) // Random -> Handle Protoss as standard
		enemyRace = Races::Protoss;

	filename.append(enemyRace.c_str());
	InitializePrior(filename, enemyRace, false); // Works for random as well, but Protoss is default anyway
	
	if (Broodwar->enemy()->getRace() != Races::Protoss
		&& Broodwar->enemy()->getRace() != Races::Terran
		&& Broodwar->enemy()->getRace() != Races::Zerg) // Random
	{
		// Protoss is handled above

		// Terran
		filename_TvP.append(Races::Terran.c_str());
		InitializePrior(filename_TvP, Races::Terran, true);
		
		// Zerg
		filename_ZvP.append(Races::Zerg.c_str());
		InitializePrior(filename_ZvP, Races::Zerg, true);
	}
}

PredictionManager::~PredictionManager()
{
}

bool PredictionManager::alreadySaw(UnitType ut)
{
	bool ret = (_alreadySawUnitTypes.find(ut) != _alreadySawUnitTypes.end());
	if (!ret)
		_alreadySawUnitTypes.insert(ut);
	return ret;
}

void PredictionManager::SwapRaceOnFound(Race r)
{
	if (r == Races::Terran)
	{
		serializedTables.swap(TvP);
		op_prior.swap(op_prior_TvP);
	}
	else if (r == Races::Zerg)
	{
		serializedTables.swap(ZvP);
		op_prior.swap(op_prior_ZvP);
	}
	size_t nbOpenings = serializedTables.openings.size();
	openingsProbas = std::vector<long double>(nbOpenings, 1.0 / nbOpenings);
}

#ifdef PREDICTION_MANAGER_DEBUG
void PredictionManager::drawPredictionInformation()
{
	int x = 490;
	int y = 148;
	int barWidth = 40;
	

	std::set<int> dtRush;
	dtRush.insert(Protoss_Nexus);
	dtRush.insert(Protoss_Assimilator);
	dtRush.insert(Protoss_Pylon);
	dtRush.insert(Protoss_Gateway);
	dtRush.insert(Protoss_Cybernetics_Core);
	dtRush.insert(Protoss_Citadel_of_Adun);
	//dtRush.insert(Protoss_Templar_Archives);
	dtRush.insert(Protoss_Pylon2);
	dtRush.insert(Protoss_Gateway2);
	
	if (!tableLoaded)
		return;

	std::vector<long double> tmpOpProb(openingsProbas);
	tmpOpProb = computeVecDistribOpenings(BWAPI::Broodwar->elapsedTime() + PRED_ADV_TIME, buildingsTypesSeen);
	if (PRED_ADV_TIME > 0)
	{
		Broodwar->drawTextScreen(480, 150, "Strategy Prediction t+%d (percent)", PRED_ADV_TIME);
	}
	else
	{
		Broodwar->drawTextScreen(510, 150, "Strategy Estimation (percent)");
	}

	int bottom = 170 + 18 * (tmpOpProb.size() + 1);

	BWAPI::Broodwar->drawBoxScreen(x, y, x + 110 + barWidth, bottom, BWAPI::Colors::Black, true);
	Broodwar->drawLineScreen(490, 148, 630, 148, BWAPI::Colors::Blue);
	Broodwar->drawLineScreen(490, 148, 490, bottom, BWAPI::Colors::Blue);
	Broodwar->drawLineScreen(630, 148, 630, bottom, BWAPI::Colors::Blue);
	Broodwar->drawLineScreen(490, bottom, 630, bottom, BWAPI::Colors::Blue);
	for (size_t i = 0; i < tmpOpProb.size(); ++i)
		Broodwar->drawTextScreen(500, 170 + 18 * i, "%s ==> %Lg", serializedTables.openings[i].c_str(), tmpOpProb[i] * 100);

	int i = 0;
	for (std::set<int>::const_iterator it = buildingsTypesSeen.begin(); it != buildingsTypesSeen.end(); ++it)
	{
		Broodwar->drawTextScreen(500 + 8 * i, 170 + 18 * tmpOpProb.size(), "%d", *it);
		++i;
	}
}
#endif

void PredictionManager::onFrame()
{
	std::string isFinal = InformationManager::Instance().getFirstTimeInvis() ? "yea, first time invis" : "not first time invis";
	BWAPI::Broodwar->drawTextScreen(300, 300, isFinal.c_str());

	if (BWAPI::Broodwar->elapsedTime() > 3 * 60 && !StrategyManager::Instance().isStratFinal())
	{
		std::vector<long double> tmpOpProb = computeVecDistribOpenings(BWAPI::Broodwar->elapsedTime() + PRED_ADV_TIME, buildingsTypesSeen);
		std::sort(tmpOpProb.begin(), tmpOpProb.end());

		if (*tmpOpProb.rbegin() > 0.5)
		{
			ProductionManager::Instance().forceResearch();
		}
	}
}
void PredictionManager::onUnitShow(Unit* u)
{
	if (Broodwar->getFrameCount() / 24 >= LEARNED_TIME_LIMIT || u->getPlayer()->isNeutral())
		return;

	if (u->getPlayer()->isEnemy(Broodwar->self()) && !alreadySaw(u->getType()))
	{
		std::string name = u->getType().getName();
		
		/// If it's the first enemy unit that we see and he is random,
		/// update st with the correct serialized_table
		if (!tableLoaded)
		{
			SwapRaceOnFound(u->getType().getRace());
			tableLoaded = true;
		}

		int recomputeTime = 0;
		if (u->getType().isBuilding() || u->getType() == UnitTypes::Zerg_Overlord)
		{
			/// We are interested in the time at which the construction began
			if (insertBuilding(u))
			{
				if (u->isCompleted())
					recomputeTime = (Broodwar->getFrameCount() - u->getType().buildTime()) / 24;
				else
					recomputeTime = (Broodwar->getFrameCount() - (u->getType().buildTime() - u->getRemainingBuildTime())) / 24;
			}
		}
		else
		{
			/// We infer the buildings needed to produce this unit
			for (std::map<UnitType, int>::const_iterator it = u->getType().requiredUnits().begin(); it != u->getType().requiredUnits().end(); ++it)
			{
				// Have we seen this required unit before?
				if (insertBuilding(it->first))
				{
					/// The latest he could have built this unit
					int tmpTime = (Broodwar->getFrameCount()
						- (it->first.buildTime()
						- u->getType().buildTime() // minimum build time
						//+ u->getDistance(enemyStart, u->getPosition()) / u->getType().topSpeed() // minimum walking distance done next line (approx.)
						- static_cast<int>(((Broodwar->mapWidth() + Broodwar->mapHeight()) / 2.0 * TILE_SIZE) / u->getType().topSpeed())
						)) / 24;
					if (!recomputeTime || tmpTime < recomputeTime) // we do only one recompute (the final) instead of many, for each buildings
						recomputeTime = tmpTime;
				}
			}
		}

		// Recompute only if there was a new unit
		if (recomputeTime)
		{
			computeDistribOpenings(recomputeTime, buildingsTypesSeen);
		}
	}
}

void PredictionManager::onUnitHide(Unit* u)
{

}
void PredictionManager::onUnitDestroy(Unit* u)
{

}
void PredictionManager::onStart()
{
	Broodwar->printf("Prediction manager started!");
}

std::vector<long double> PredictionManager::computeDistribOpenings(int time, const std::set<int>& observations, bool computePrior)
{
	/// time in seconds
	if (time >= LEARNED_TIME_LIMIT || time <= 0)
	{
		// Return uniform distribution
		return std::vector<long double>(openingsProbas.size(), 1.0 / openingsProbas.size());
	}

	openingsProbas = computeVecDistribOpenings(time, buildingsTypesSeen);

	if (computePrior)
	{
		int t = format_max_time(time);
		for (size_t i = 0; i < openingsProbas.size(); ++i)
		{
			// cumulative moving average: ca_{n+1} = (x_{n+1}+n*ca_{n})/(n+1)
			op_prior.tabulated_P_Op_knowing_Time[t][i]
				= (openingsProbas[i] + op_prior.numberGames[t] * op_prior.tabulated_P_Op_knowing_Time[t][i])
				/ (op_prior.numberGames[t] + 1);
			op_prior.numberGames[t] += 1;
		}
	}
	return openingsProbas;
}
std::vector<long double> PredictionManager::computeDistribOpenings(int time, const std::set<int>& observations)
{
	// Recompute prior only if we are inferring based on current game state - not speculating
	return  computeDistribOpenings(time, observations, buildingsTypesSeen == observations);
}

// Computes probabilities of openings
std::vector<long double> PredictionManager::computeVecDistribOpenings(int time, const std::set<int>& observations)
{
	// Initialize as a uniform distribution
	std::vector<long double> ret(openingsProbas.size(), 1.0 / openingsProbas.size());
	/// time in seconds
	if (time >= LEARNED_TIME_LIMIT || time <= 0)
	{
		return ret; // we don't know and thus give back the uniform distribution
	}

	size_t nbXes = serializedTables.vector_X.size();
	std::vector<unsigned int> compatibleXes;
	
	// Select Build trees compatible with observations from all learned build trees
	for (size_t i = 0; i < nbXes; ++i)
	{
		if (testBuildTreePossible(i, observations))
			compatibleXes.push_back(i);
	}

	long double runningSum = 0.0;
	// Each opening has a probability summed of probabilities of all build trees at a given time
	for (size_t i = 0; i < openingsProbas.size(); ++i)
	{
		long double sumX = MIN_PROB;
		for (std::vector<unsigned int>::const_iterator it = compatibleXes.begin(); it != compatibleXes.end(); ++it)
		{ /// perhaps underflow? log-prob?
			sumX += op_prior.tabulated_P_Op_knowing_Time[format_max_time(time)][i]
				* serializedTables.tabulated_P_X_Op[(*it) * openingsProbas.size() + i]
				* serializedTables.tabulated_P_Time_X_Op[(*it) * openingsProbas.size() * LEARNED_TIME_LIMIT + i * LEARNED_TIME_LIMIT + time];
		}
		ret[i] *= sumX;
		runningSum += ret[i];
	}
	long double verifSum = 0.0;

	// Normalize probabilities
	for (size_t i = 0; i < openingsProbas.size(); ++i)
	{
		ret[i] /= runningSum;
		if (ret[i] != ret[i] // test for NaN / 1#IND
			//|| openingsProbas[i] < MIN_PROB        // min proba
			)
		{
			ret[i] = MIN_PROB;
		}
		verifSum += ret[i];
	}

	// Verify that sum of probabilities is in given error range
	if (verifSum < 0.99 || verifSum > 1.01)
	{
		for (size_t i = 0; i < openingsProbas.size(); ++i)
			ret[i] /= verifSum;
	}

	return ret;
}

bool PredictionManager::testBuildTreePossible(int indBuildTree, const std::set<int>& setObs)
{
	// Go through all observations
	// If we saw something that is not in the BT return false -> this BT is not possible
	for (std::set<int>::const_iterator it = setObs.begin(); it != setObs.end(); ++it)
	{
		// Ignore all units we don't have build tree for
		if (!serializedTables.vector_X[indBuildTree].count(*it) && !missingUnitTypes.count(*it))
			return false;
	}
	return true;
}

std::vector<BuildTreeProbaPair> PredictionManager::GetP_BT_T(int time, const std::vector<size_t>& buildTrees)
{
	long double BASE_PROBA = 1.0 / buildTrees.size(); // Simulate init as uniform distribution
	std::vector<BuildTreeProbaPair> probabilities;
	long double runningSum = 0.0;

	for (std::vector<size_t>::const_iterator bt = buildTrees.begin(); bt != buildTrees.end(); ++bt)
	{
		long double proba = MIN_PROB;
		//set<int> buildings = serializedTables.vector_X[*bt];

		// For each Opening
		for (size_t i = 0; i < openingsProbas.size(); ++i)
		{
			proba +=
				op_prior.tabulated_P_Op_knowing_Time[format_max_time(time)][i] * // P(OP|T) = prior
				serializedTables.tabulated_P_X_Op[(*bt) * openingsProbas.size() + i] * // P(BT|OP)
				serializedTables.tabulated_P_Time_X_Op[(*bt) * openingsProbas.size() * LEARNED_TIME_LIMIT + i * LEARNED_TIME_LIMIT + time]; // P(T|BT, OP)
		}

		probabilities.push_back(BuildTreeProbaPair(*bt, BASE_PROBA * proba));
		runningSum += BASE_PROBA * proba;
	}

	long double verifSum = 0.0;
	// Normalize probabilities
	for (size_t i = 0; i < probabilities.size(); ++i)
	{
		probabilities[i].probability /= runningSum;
		if (probabilities[i].probability != probabilities[i].probability) // test for NaN / 1#IND
		{
			probabilities[i].probability = MIN_PROB;
		}
		verifSum += probabilities[i].probability;
	}

	// Verify that sum of probabilities is in given error range
	if (verifSum < 0.99 || verifSum > 1.01)
	{
		for (size_t i = 0; i < openingsProbas.size(); ++i)
			probabilities[i].probability /= verifSum;
	}

	return probabilities;
}

double PredictionManager::ComputeDistribChangeProba(const int time)
{
	long double proba = 0.0;

	// Get all possible build trees compatible with current observations
	size_t nbXes = serializedTables.vector_X.size();
	std::vector<size_t> compatibleXes;
	for (size_t i = 0; i < nbXes; ++i)
	{
		if (testBuildTreePossible(i, buildingsTypesSeen))
			compatibleXes.push_back(i);
	}
	
	// Get probability that our opponent has given build tree at given time - for each BT
	std::vector<BuildTreeProbaPair> BTProbas = GetP_BT_T(time + 30, compatibleXes);

	// Get the most probable opening right now
	std::vector<OpeningProbaPair> currentDistrib = GetOpeningProbas(buildingsTypesSeen, time);
	sort(currentDistrib.begin(), currentDistrib.end(), OpeningProbaPair::descending);
	OpeningProbaPair currentOpening = currentDistrib.front();

	for (std::vector<BuildTreeProbaPair>::const_iterator bt = BTProbas.begin(); bt != BTProbas.end(); ++bt)
	{
		std::vector<OpeningProbaPair> newDistrib = GetOpeningProbas(serializedTables.vector_X[bt->index], time + 30);
		sort(newDistrib.begin(), newDistrib.end(), OpeningProbaPair::descending);
		OpeningProbaPair newOpening = newDistrib.front();
		
		proba += newOpening.opening != currentOpening.opening ? bt->probability : 0;
	}

	return static_cast<double>(proba);
}

const std::vector<OpeningProbaPair> PredictionManager::GetOpeningProbas(const std::set<int>& observations, int time)
{
	std::vector<long double> probas = computeDistribOpenings(time, observations);
	std::vector<OpeningProbaPair> openProba;
	for (size_t i = 0; i < probas.size(); ++i)
		openProba.push_back(OpeningProbaPair(serializedTables.openings[i], probas[i]));

	return openProba;
}

int PredictionManager::format_max_time(int t)
{
	return std::min((LEARNED_TIME_LIMIT - 1) / 60, t / 60);
}

const std::vector<OpeningProbaPair> PredictionManager::GetOpeningProbas(int time)
{
	return GetOpeningProbas(buildingsTypesSeen, time);
}
// Returns true if ut is yet unseen type (= ut is a new type)
bool PredictionManager::insertBuilding(UnitType ut)
{
	size_t previous_size = buildingsTypesSeen.size();
	if (ut.getRace() == Races::Protoss)
	{
		if (ut == UnitTypes::Protoss_Robotics_Facility)
			buildingsTypesSeen.insert(Protoss_Robotics_Facility);
		else if (ut == UnitTypes::Protoss_Observatory)
			buildingsTypesSeen.insert(Protoss_Observatory);
		else if (ut == UnitTypes::Protoss_Gateway)
			buildingsTypesSeen.insert(Protoss_Gateway);
		else if (ut == UnitTypes::Protoss_Citadel_of_Adun)
			buildingsTypesSeen.insert(Protoss_Citadel_of_Adun);
		else if (ut == UnitTypes::Protoss_Cybernetics_Core)
			buildingsTypesSeen.insert(Protoss_Cybernetics_Core);
		else if (ut == UnitTypes::Protoss_Templar_Archives)
			buildingsTypesSeen.insert(Protoss_Templar_Archives);
		else if (ut == UnitTypes::Protoss_Forge)
			buildingsTypesSeen.insert(Protoss_Forge);
		else if (ut == UnitTypes::Protoss_Stargate)
			buildingsTypesSeen.insert(Protoss_Stargate);
		else if (ut == UnitTypes::Protoss_Fleet_Beacon)
			buildingsTypesSeen.insert(Protoss_Fleet_Beacon);
		else if (ut == UnitTypes::Protoss_Arbiter_Tribunal)
			buildingsTypesSeen.insert(Protoss_Arbiter_Tribunal);
		else if (ut == UnitTypes::Protoss_Robotics_Support_Bay)
			buildingsTypesSeen.insert(Protoss_Robotics_Support_Bay);
	}
	else if (ut.getRace() == Races::Terran)
	{
		if (ut == UnitTypes::Terran_Comsat_Station)
			buildingsTypesSeen.insert(Terran_Comsat_Station);
		else if (ut == UnitTypes::Terran_Nuclear_Silo)
			buildingsTypesSeen.insert(Terran_Nuclear_Silo);
		else if (ut == UnitTypes::Terran_Barracks)
			buildingsTypesSeen.insert(Terran_Barracks);
		else if (ut == UnitTypes::Terran_Academy)
			buildingsTypesSeen.insert(Terran_Academy);
		else if (ut == UnitTypes::Terran_Factory)
			buildingsTypesSeen.insert(Terran_Factory);
		else if (ut == UnitTypes::Terran_Starport)
			buildingsTypesSeen.insert(Terran_Starport);
		else if (ut == UnitTypes::Terran_Control_Tower)
			buildingsTypesSeen.insert(Terran_Control_Tower);
		else if (ut == UnitTypes::Terran_Science_Facility)
			buildingsTypesSeen.insert(Terran_Science_Facility);
		else if (ut == UnitTypes::Terran_Covert_Ops)
			buildingsTypesSeen.insert(Terran_Covert_Ops);
		else if (ut == UnitTypes::Terran_Physics_Lab)
			buildingsTypesSeen.insert(Terran_Physics_Lab);
		else if (ut == UnitTypes::Terran_Machine_Shop)
			buildingsTypesSeen.insert(Terran_Machine_Shop);
		else if (ut == UnitTypes::Terran_Engineering_Bay)
			buildingsTypesSeen.insert(Terran_Engineering_Bay);
		else if (ut == UnitTypes::Terran_Armory)
			buildingsTypesSeen.insert(Terran_Armory);
		else if (ut == UnitTypes::Terran_Missile_Turret)
			buildingsTypesSeen.insert(Terran_Missile_Turret);
	}
	else if (ut.getRace() == Races::Zerg)
	{
		if (ut == UnitTypes::Zerg_Lair)
			buildingsTypesSeen.insert(Zerg_Lair);
		else if (ut == UnitTypes::Zerg_Hive)
			buildingsTypesSeen.insert(Zerg_Hive);
		else if (ut == UnitTypes::Zerg_Nydus_Canal)
			buildingsTypesSeen.insert(Zerg_Nydus_Canal);
		else if (ut == UnitTypes::Zerg_Hydralisk_Den)
			buildingsTypesSeen.insert(Zerg_Hydralisk_Den);
		else if (ut == UnitTypes::Zerg_Defiler_Mound)
			buildingsTypesSeen.insert(Zerg_Defiler_Mound);
		else if (ut == UnitTypes::Zerg_Greater_Spire)
			buildingsTypesSeen.insert(Zerg_Greater_Spire);
		else if (ut == UnitTypes::Zerg_Queens_Nest)
			buildingsTypesSeen.insert(Zerg_Queens_Nest);
		else if (ut == UnitTypes::Zerg_Evolution_Chamber)
			buildingsTypesSeen.insert(Zerg_Evolution_Chamber);
		else if (ut == UnitTypes::Zerg_Ultralisk_Cavern)
			buildingsTypesSeen.insert(Zerg_Ultralisk_Cavern);
		else if (ut == UnitTypes::Zerg_Spire)
			buildingsTypesSeen.insert(Zerg_Spire);
		else if (ut == UnitTypes::Zerg_Spawning_Pool)
			buildingsTypesSeen.insert(Zerg_Spawning_Pool);
		else if (ut == UnitTypes::Zerg_Creep_Colony)
			buildingsTypesSeen.insert(Zerg_Creep_Colony);
	}
	else
	{
		Broodwar->printf("ERROR: PredictionManager failed to determine the race of %s", ut.getName().c_str());
	}

	return buildingsTypesSeen.size() > previous_size;
}
// Returns true if u is of yet unseen type (= u is a new type)
bool PredictionManager::insertBuilding(Unit* u)
{
	UnitType ut = u->getType();
	size_t previous_size = buildingsTypesSeen.size();
	if (ut.getRace() == Races::Protoss)
	{
		if (ut == UnitTypes::Protoss_Nexus)
		{
			int tmp = Protoss_Nexus;
			while (buildingsTypesSeen.count(tmp))
				++tmp;
			if (tmp <= Protoss_Expansion)
				buildingsTypesSeen.insert(tmp);
		}
		else if (ut == UnitTypes::Protoss_Robotics_Facility)
			buildingsTypesSeen.insert(Protoss_Robotics_Facility);
		else if (ut == UnitTypes::Protoss_Pylon)
		{
			int tmp = Protoss_Pylon;
			while (buildingsTypesSeen.count(tmp))
				++tmp;
			if (tmp <= Protoss_Pylon2)
				buildingsTypesSeen.insert(tmp);
		}
		else if (ut == UnitTypes::Protoss_Assimilator)
		{
			int tmp = Protoss_Assimilator;
			while (buildingsTypesSeen.count(tmp))
				++tmp;
			if (tmp <= Protoss_Assimilator)
				buildingsTypesSeen.insert(tmp);
		}
		else if (ut == UnitTypes::Protoss_Observatory)
			buildingsTypesSeen.insert(Protoss_Observatory);
		else if (ut == UnitTypes::Protoss_Gateway)
		{
			int tmp = Protoss_Gateway;
			while (buildingsTypesSeen.count(tmp))
				++tmp;
			if (tmp <= Protoss_Gateway2)
				buildingsTypesSeen.insert(tmp);
		}
		else if (ut == UnitTypes::Protoss_Photon_Cannon)
			buildingsTypesSeen.insert(Protoss_Photon_Cannon);
		else if (ut == UnitTypes::Protoss_Citadel_of_Adun)
			buildingsTypesSeen.insert(Protoss_Citadel_of_Adun);
		else if (ut == UnitTypes::Protoss_Cybernetics_Core)
			buildingsTypesSeen.insert(Protoss_Cybernetics_Core);
		else if (ut == UnitTypes::Protoss_Templar_Archives)
			buildingsTypesSeen.insert(Protoss_Templar_Archives);
		else if (ut == UnitTypes::Protoss_Forge)
			buildingsTypesSeen.insert(Protoss_Forge);
		else if (ut == UnitTypes::Protoss_Stargate)
			buildingsTypesSeen.insert(Protoss_Stargate);
		else if (ut == UnitTypes::Protoss_Fleet_Beacon)
			buildingsTypesSeen.insert(Protoss_Fleet_Beacon);
		else if (ut == UnitTypes::Protoss_Arbiter_Tribunal)
			buildingsTypesSeen.insert(Protoss_Arbiter_Tribunal);
		else if (ut == UnitTypes::Protoss_Robotics_Support_Bay)
			buildingsTypesSeen.insert(Protoss_Robotics_Support_Bay);
		else if (ut == UnitTypes::Protoss_Shield_Battery)
			buildingsTypesSeen.insert(Protoss_Shield_Battery);
	}
	else if (ut.getRace() == Races::Terran)
	{
		if (ut == UnitTypes::Terran_Command_Center)
		{
			int tmp = Terran_Command_Center;
			while (buildingsTypesSeen.count(tmp))
				++tmp;
			if (tmp <= Terran_Expansion)
				buildingsTypesSeen.insert(tmp);
		}
		else if (ut == UnitTypes::Terran_Comsat_Station)
			buildingsTypesSeen.insert(Terran_Comsat_Station);
		else if (ut == UnitTypes::Terran_Nuclear_Silo)
			buildingsTypesSeen.insert(Terran_Nuclear_Silo);
		else if (ut == UnitTypes::Terran_Supply_Depot)
		{
			int tmp = Terran_Supply_Depot;
			while (buildingsTypesSeen.count(tmp))
				++tmp;
			if (tmp <= Terran_Supply_Depot)
				buildingsTypesSeen.insert(tmp);
		}
		else if (ut == UnitTypes::Terran_Refinery)
		{
			int tmp = Terran_Refinery;
			while (buildingsTypesSeen.count(tmp))
				++tmp;
			if (tmp <= Terran_Refinery)
				buildingsTypesSeen.insert(tmp);
		}
		else if (ut == UnitTypes::Terran_Barracks)
		{
			int tmp = Terran_Barracks;
			while (buildingsTypesSeen.count(tmp))
				++tmp;
			if (tmp <= Terran_Barracks2)
				buildingsTypesSeen.insert(tmp);
		}
		else if (ut == UnitTypes::Terran_Academy)
			buildingsTypesSeen.insert(Terran_Academy);
		else if (ut == UnitTypes::Terran_Factory)
			buildingsTypesSeen.insert(Terran_Factory);
		else if (ut == UnitTypes::Terran_Starport)
			buildingsTypesSeen.insert(Terran_Starport);
		else if (ut == UnitTypes::Terran_Control_Tower)
			buildingsTypesSeen.insert(Terran_Control_Tower);
		else if (ut == UnitTypes::Terran_Science_Facility)
			buildingsTypesSeen.insert(Terran_Science_Facility);
		else if (ut == UnitTypes::Terran_Covert_Ops)
			buildingsTypesSeen.insert(Terran_Covert_Ops);
		else if (ut == UnitTypes::Terran_Physics_Lab)
			buildingsTypesSeen.insert(Terran_Physics_Lab);
		else if (ut == UnitTypes::Terran_Machine_Shop)
			buildingsTypesSeen.insert(Terran_Machine_Shop);
		else if (ut == UnitTypes::Terran_Engineering_Bay)
			buildingsTypesSeen.insert(Terran_Engineering_Bay);
		else if (ut == UnitTypes::Terran_Armory)
			buildingsTypesSeen.insert(Terran_Armory);
		else if (ut == UnitTypes::Terran_Missile_Turret)
			buildingsTypesSeen.insert(Terran_Missile_Turret);
		else if (ut == UnitTypes::Terran_Bunker)
			buildingsTypesSeen.insert(Terran_Bunker);
	}
	else if (ut.getRace() == Races::Zerg)
	{
		if (ut == UnitTypes::Zerg_Hatchery)
		{
			int tmp = Zerg_Hatchery;
			while (buildingsTypesSeen.count(tmp))
				++tmp;
			if (tmp <= Zerg_Expansion2)
				buildingsTypesSeen.insert(tmp);
		}
		else if (ut == UnitTypes::Zerg_Lair)
			buildingsTypesSeen.insert(Zerg_Lair);
		else if (ut == UnitTypes::Zerg_Hive)
			buildingsTypesSeen.insert(Zerg_Hive);
		else if (ut == UnitTypes::Zerg_Nydus_Canal)
			buildingsTypesSeen.insert(Zerg_Nydus_Canal);
		else if (ut == UnitTypes::Zerg_Hydralisk_Den)
			buildingsTypesSeen.insert(Zerg_Hydralisk_Den);
		else if (ut == UnitTypes::Zerg_Defiler_Mound)
			buildingsTypesSeen.insert(Zerg_Defiler_Mound);
		else if (ut == UnitTypes::Zerg_Greater_Spire)
			buildingsTypesSeen.insert(Zerg_Greater_Spire);
		else if (ut == UnitTypes::Zerg_Queens_Nest)
			buildingsTypesSeen.insert(Zerg_Queens_Nest);
		else if (ut == UnitTypes::Zerg_Evolution_Chamber)
			buildingsTypesSeen.insert(Zerg_Evolution_Chamber);
		else if (ut == UnitTypes::Zerg_Ultralisk_Cavern)
			buildingsTypesSeen.insert(Zerg_Ultralisk_Cavern);
		else if (ut == UnitTypes::Zerg_Spire)
			buildingsTypesSeen.insert(Zerg_Spire);
		else if (ut == UnitTypes::Zerg_Spawning_Pool)
			buildingsTypesSeen.insert(Zerg_Spawning_Pool);
		else if (ut == UnitTypes::Zerg_Creep_Colony)
			buildingsTypesSeen.insert(Zerg_Creep_Colony);
		else if (ut == UnitTypes::Zerg_Spore_Colony)
			buildingsTypesSeen.insert(Zerg_Spore_Colony);
		else if (ut == UnitTypes::Zerg_Sunken_Colony)
			buildingsTypesSeen.insert(Zerg_Sunken_Colony);
		else if (ut == UnitTypes::Zerg_Extractor)
		{
			int tmp = Zerg_Extractor;
			while (buildingsTypesSeen.count(tmp))
				++tmp;
			if (tmp <= Zerg_Extractor)
				buildingsTypesSeen.insert(tmp);
		}
		else if (ut == UnitTypes::Zerg_Overlord)
		{
			if (notFirstOverlord)
			{
				int tmp = Zerg_Overlord;
				while (buildingsTypesSeen.count(tmp))
					++tmp;
				if (tmp <= Zerg_Building_Overlord)
					buildingsTypesSeen.insert(tmp);
			}
			else
				notFirstOverlord = true;
		}
	}
	else
	{
		Broodwar->printf("ERROR: PredictionManager failed to determine the race of %s", ut.getName().c_str());
	}
	
	if (buildingsTypesSeen.size() > previous_size)
		return true;
	else
		return false;
}