#pragma once

#include <list>
#include "Common.h"
#include "MapGrid.h"

namespace AdvScout
{
	typedef std::list<BWAPI::Position> ScoutPath;
}

/** Represents a wrapper to the BWAPI::Unit class.
*	ScoutUnit has advanced scouting capabilities.
*	When given path and updated regularly simulates a scouting FSM.
*/
class ScoutUnit
{
public:
	/** All possible states of the scouting FSM */
	enum ScoutState
	{
		Idle = 0,
		GoingToScout,
		InEnemyBase,
		CommingFromMission
	};

	ScoutUnit() :
		unit(NULL),
		state(Idle),
		age(0),
		lastUpdateTime(0)
	{}

	ScoutUnit(BWAPI::Unit * scout) :
		unit(scout),
		state(Idle),
		age(0),
		lastUpdateTime(0)
	{}

	/** Sets given path as current path to follow on scouting mission when updated */
	inline void SetPath(const AdvScout::ScoutPath& newPath) { path = newPath; currentNode = path.begin(); }
	
	/** Returns current state of the scouting FSM */
	inline ScoutState GetState() const { return state; }
	
	/** Returns current scouting unit */
	inline const BWAPI::Unit* GetUnit() const { return unit; }

	/** Resets the scouting FSM to start new mission with current path */
	inline void StartMission() { state = ScoutState::GoingToScout; }
	
	/** Returns whether the unit is valid ScoutingUnit. 
	*	Both scouting validity and overall BWAPI validity is tested. 
	*/
	inline bool IsValid() const
	{
		return
			unit
			&& unit->getHitPoints() > 0
			&& unit->exists()
			&& age < MAX_AGE;
	}

	/** Event handler to update the unit on regular basis. Should be registered to update on each frame. */
	void Update();

private:
	/** Max age of any scouting unit. Older units are disposed. */
	const static int MAX_AGE = 3 * 60;

	/** The unit we are wrapping and which is used for scouting */
	BWAPI::Unit * unit;

	/** Current scouting path to follow on scouting mission. */
	AdvScout::ScoutPath path;

	/** Current state of the scouti */
	ScoutState state;
	/** Current node we are going to in the scouting path. */
	AdvScout::ScoutPath::const_iterator currentNode;
	/** Current node when returning to base in the scouting path */
	AdvScout::ScoutPath::const_reverse_iterator currentBackNode;

	/** Time of last update from the beginning of the game. Used to compute deltaTime. */
	int lastUpdateTime;

	/** Current age of the unit. Units too old are disposed. */
	int age;

#ifdef ADV_SCOUT_DEBUG
	/** Draws debug interface on the screen in game */
	void DrawDebug() const;
#endif
};

/** Takes care of advanced scouting routines.
*	Does not replace the regular ScoutManager.
*	Can be updated and given set of a singe unit may perform advanced scouting.
*/
class AdvancedScoutManager
{
	/** Pathfinding typedefs */
	typedef std::map <GridCell*, GridCell*> Preceding;
	typedef std::list <PathCell> Opened;

	/** Our current scouting unit */
	ScoutUnit scout;

	/** Issues orders to current scouting unit. */
	void moveScouts();

	/** Performs A* pathfinding to find safe path to enemy base. Uses MapGrid to get danger levels over the map. */
	AdvScout::ScoutPath FindSafePath(const ScoutUnit& scout, const BWTA::BaseLocation* enemyBase) const;
	/** Returns the actual path given the goal and preceding cells from pathfinding. */
	AdvScout::ScoutPath GeneratePath(const PathCell &to, const Preceding &preceding) const;

	/** All scout types we recognize ordered from the best to the worst */
	enum ScoutType
	{
		// Currently playing only Protoss
		Corsair = 0,
		Scout,
		Zealot,
		Probe,
		Air,
		Land,
		Observer // Do not use observer for scouting since invis detection is much more important
	};
	
	/** Returns which of given units is better scout according to the manager. */
	static const bool isBetterScout(const BWAPI::Unit* newScout, const BWAPI::Unit* oldScout);

	/** Returns what type of scout is given unit. */
	static const ScoutType getScoutType(const BWAPI::Unit* scout);

public:
	/** Update event handler. Given set of a single unit performs advanced scouting if needed. */
	void update(const std::set<BWAPI::Unit *> & workerScoutUnits);

	/**	As long as we do not want to dispose of current scoutUnit - we should be able to keep it - gameCommander should not take it away from us
	*	If we want scout with one unit until it has low enough HP or it has been on too many scouting missions this may be the place to implement it
	*/
	inline bool ShouldDisposeOfCurrentScout() const { return true; }

	/** Returns true if currently on scouting mission with valid scout */
	inline bool IsScouting() const
	{
		return
			scout.IsValid()
			&& scout.GetState() != ScoutUnit::Idle
			&& scout.GetState() != ScoutUnit::CommingFromMission;
	}

	/** Returns a unit that is best suited for scouting from given set of units. 
		Returns NULL if there are no available units */
	static BWAPI::Unit* FindBestScout(const std::set<BWAPI::Unit*>& units);

	/** Returns true if given unit is our current scouting unit */
	bool isCurrentScout(BWAPI::Unit* u) { return scout.GetUnit() == u; }
};