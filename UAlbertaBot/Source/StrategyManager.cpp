#include "Common.h"
#include "StrategyManager.h"
#include <algorithm>

// constructor
StrategyManager::StrategyManager()
	: firstAttackSent(false)
	, currentStrategy(0)
	, selfRace(BWAPI::Broodwar->self()->getRace())
	, enemyRace(BWAPI::Broodwar->enemy()->getRace())
	, hasFinalStrategy(false)
	, firstTimeInvis(true)
	, NEW_SCOUT_TIME_THOLD(3*60)
	, OP_CHANGE_PROBA_THOLD(0.33)
	, EARLIEST_ADV_SCOUT_TIME(4*60)
	, GROUND_SCOUT_TIME_THOLD(8*60)
	, BUILD_FLY_SCOUT_TIME(12*60)
	, ADD_UNITS_TO_STRAT(8*60)
{
	addStrategies();
	setStrategy();
}
StrategyManager::~StrategyManager()
{
}
// get an instance of this
StrategyManager & StrategyManager::Instance() 
{
	static StrategyManager instance;
	return instance;
}

void StrategyManager::addStrategies() 
{
	// We are playing only protoss race
	protossOpeningBook = std::vector<std::string>(NumProtossStrategies);
#if ORIGINAL_BO
    protossOpeningBook[ProtossZealotRush]	= "0 0 0 0 1 0 3 3 0 0 4 1 4 4 0 4 4 0 1 4 3 0 1 0 4 0 4 4 4 4 1 0 4 4 4";
    protossOpeningBook[ProtossDarkTemplar]	= "0 0 0 0 1 0 3 0 7 0 5 0 12 0 13 3 22 22 1 22 22 0 1 0";
	protossOpeningBook[ProtossDragoons]	    = "0 0 0 0 1 0 0 3 0 7 0 0 5 0 0 3 8 6 1 6 6 0 3 1 0 6 6 6";
#endif
	stdOpening								= "0 0 0 0 1 0 0 3 5 0 4 7 0 0 6";
	//stdOpening = "0 0 0 0 1 0 3 0 0 5 4 1 4 0 4 0 7 0 0 4 0 1 0 4 0";
	
	results = std::vector<IntPair>(NumProtossStrategies);

#if ORIGINAL_BO
	// Prepare for oponent
	else if (enemyRace == BWAPI::Races::Protoss)
	{
		usableStrategies.push_back(ProtossZealotRush);
		usableStrategies.push_back(ProtossDarkTemplar);
		usableStrategies.push_back(ProtossDragoons);
	}
	else if (enemyRace == BWAPI::Races::Terran)
	{
		usableStrategies.push_back(ProtossZealotRush);
		usableStrategies.push_back(ProtossDarkTemplar);
		usableStrategies.push_back(ProtossDragoons);
	}
	else if (enemyRace == BWAPI::Races::Zerg)
	{
		usableStrategies.push_back(ProtossZealotRush);
		usableStrategies.push_back(ProtossDragoons);
	}
	else // Random - go for standard play
	{
		BWAPI::Broodwar->printf("Enemy Race Unknown");
		usableStrategies.push_back(ProtossZealotRush);
		usableStrategies.push_back(ProtossDragoons);
	}

	if (Options::Modules::USING_STRATEGY_IO)
	{
		readResults();
	}
#endif
}

void StrategyManager::readResults()
{
	// read in the name of the read and write directories from settings file
	struct stat buf;

	// if the file doesn't exist something is wrong so just set them to default settings
	if (stat(Options::FileIO::FILE_SETTINGS, &buf) == -1)
	{
		readDir = "bwapi-data/testio/read/";
		writeDir = "bwapi-data/testio/write/";
	}
	else
	{
		std::ifstream f_in(Options::FileIO::FILE_SETTINGS);
		getline(f_in, readDir);
		getline(f_in, writeDir);
		f_in.close();
	}

	// the file corresponding to the enemy's previous results
	std::string readFile = readDir + BWAPI::Broodwar->enemy()->getName() + ".txt";

	// if the file doesn't exist, set the results to zeros
	if (stat(readFile.c_str(), &buf) == -1)
	{
		std::fill(results.begin(), results.end(), IntPair(0,0));
	}
	// otherwise read in the results
	else
	{
		std::ifstream f_in(readFile.c_str());
		std::string line;
#if ORIGINAL_BO
		getline(f_in, line);
		results[ProtossZealotRush].first = atoi(line.c_str());
		getline(f_in, line);
		results[ProtossZealotRush].second = atoi(line.c_str());
		getline(f_in, line);
		results[ProtossDarkTemplar].first = atoi(line.c_str());
		getline(f_in, line);
		results[ProtossDarkTemplar].second = atoi(line.c_str());
		getline(f_in, line);
		results[ProtossDragoons].first = atoi(line.c_str());
		getline(f_in, line);
		results[ProtossDragoons].second = atoi(line.c_str());
#endif
		f_in.close();
	}

	BWAPI::Broodwar->printf("Results (%s): (%d %d) (%d %d) (%d %d)", BWAPI::Broodwar->enemy()->getName().c_str(), 
		results[0].first, results[0].second, results[1].first, results[1].second, results[2].first, results[2].second);
}

void StrategyManager::writeResults()
{
	std::string writeFile = writeDir + BWAPI::Broodwar->enemy()->getName() + ".txt";
	std::ofstream f_out(writeFile.c_str());

#if ORIGINAL_BO
	f_out << results[ProtossZealotRush].first   << "\n";
	f_out << results[ProtossZealotRush].second  << "\n";
	f_out << results[ProtossDarkTemplar].first  << "\n";
	f_out << results[ProtossDarkTemplar].second << "\n";
	f_out << results[ProtossDragoons].first     << "\n";
	f_out << results[ProtossDragoons].second    << "\n";
#endif

	f_out.close();
}

// Used for initial strategy setting
void StrategyManager::setStrategy()
{
	// if we are using file io to determine strategy, do so
	if (Options::Modules::USING_STRATEGY_IO)
	{
		double bestUCB = -1;
		int bestStrategyIndex = 0;

		// UCB requires us to try everything once before using the formula
		for (size_t strategyIndex(0); strategyIndex<usableStrategies.size(); ++strategyIndex)
		{
			int sum = results[usableStrategies[strategyIndex]].first + results[usableStrategies[strategyIndex]].second;

			if (sum == 0)
			{
				currentStrategy = usableStrategies[strategyIndex];
				return;
			}
		}

		// if we have tried everything once, set the maximizing ucb value
		for (size_t strategyIndex(0); strategyIndex < usableStrategies.size(); ++strategyIndex)
		{
			double ucb = getUCBValue(usableStrategies[strategyIndex]);

			if (ucb > bestUCB)
			{
				bestUCB = ucb;
				bestStrategyIndex = strategyIndex;
			}
		}
		
		currentStrategy = usableStrategies[bestStrategyIndex];
	}
	else
	{
		if (Options::Modules::USING_PREDICTION_MANAGER && !Options::Modules::USING_RANDOM_STRATEGY)
		{
			// Use 4 gate as a default strategy - gateway units are always useful
			currentStrategy = Protoss4Gate;
		}
		else // Set random strategy
		{
			srand(static_cast<unsigned int>(time(NULL)));
			currentStrategy = rand() % NumProtossStrategies;
			BWAPI::Broodwar->printf("Using random strategy %s", getCurrentStrategyName().c_str());
		}
	}
}

void StrategyManager::onStart()
{
	Logger::Instance().log("--------------------------------------------------------");
	Logger::Instance().log("---------------- New game started ----------------------");
	Logger::Instance().log("Enemy: " + BWAPI::Broodwar->enemy()->getName());
}
void StrategyManager::onEnd(const bool isWinner)
{
	if (isWinner)
		Logger::Instance().log("Win!");
	else
		Logger::Instance().log("Lose");
	Logger::Instance().log("--------------------- Game ended -----------------------");
	Logger::Instance().log("--------------------------------------------------------");
	
	// write the win/loss data to file if we're using IO
	if (Options::Modules::USING_STRATEGY_IO)
	{
		// if the game ended before the tournament time limit
		if (BWAPI::Broodwar->getFrameCount() < Options::Tournament::GAME_END_FRAME)
		{
			if (isWinner)
			{
				results[getCurrentStrategy()].first = results[getCurrentStrategy()].first + 1;
			}
			else
			{
				results[getCurrentStrategy()].second = results[getCurrentStrategy()].second + 1;
			}
		}
		// otherwise game timed out so use in-game score
		else
		{
			if (getScore(BWAPI::Broodwar->self()) > getScore(BWAPI::Broodwar->enemy()))
			{
				results[getCurrentStrategy()].first = results[getCurrentStrategy()].first + 1;
			}
			else
			{
				results[getCurrentStrategy()].second = results[getCurrentStrategy()].second + 1;
			}
		}
		
		writeResults();
	}
}

const double StrategyManager::getUCBValue(const size_t & strategy) const
{
	double totalTrials(0);
	for (size_t s(0); s<usableStrategies.size(); ++s)
	{
		totalTrials += results[usableStrategies[s]].first + results[usableStrategies[s]].second;
	}

	double C		= 0.7;
	double wins		= results[strategy].first;
	double trials	= results[strategy].first + results[strategy].second;

	double ucb = (wins / trials) + C * sqrt(std::log(totalTrials) / trials);

	return ucb;
}

const int StrategyManager::getScore(BWAPI::Player * player) const
{
	return player->getBuildingScore() + player->getKillScore() + player->getRazingScore() + player->getUnitScore();
}

const std::string StrategyManager::getOpeningBook() const
{
	// We are playing only protoss
	if (protossOpeningBook[currentStrategy] == "")
		return stdOpening;

	return protossOpeningBook[currentStrategy];
}

// when do we want to defend with our workers?
// this function can only be called if we have no fighters to defend with
const int StrategyManager::defendWithWorkers()
{
	if (!Options::Micro::WORKER_DEFENSE)
	{
		return false;
	}

	// our home nexus position
	BWAPI::Position homePosition = BWTA::getStartLocation(BWAPI::Broodwar->self())->getPosition();;

	// enemy units near our workers
	int enemyUnitsNearWorkers = 0;

	// defense radius of nexus
	int defenseRadius = 300;

	// fill the set with the types of units we're concerned about
	BOOST_FOREACH (BWAPI::Unit * unit, BWAPI::Broodwar->enemy()->getUnits())
	{
		// if it's a zergling or a worker we want to defend
		if (unit->getType() == BWAPI::UnitTypes::Zerg_Zergling)
		{
			if (unit->getDistance(homePosition) < defenseRadius)
			{
				enemyUnitsNearWorkers++;
			}
		}
	}

	// if there are enemy units near our workers, we want to defend
	return enemyUnitsNearWorkers;
}

// called by combat commander to determine whether or not to send an attack force
// freeUnits are the units available to do this attack
const bool StrategyManager::doAttack(const std::set<BWAPI::Unit *> & freeUnits)
{
	//return true;
	int ourForceSize = (int)freeUnits.size();

	int numUnitsNeededForAttack = 2;

	bool doAttack  = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Dark_Templar) >= 1
					|| ourForceSize >= numUnitsNeededForAttack;

	if (doAttack)
	{
		firstAttackSent = true;
	}

	return doAttack || firstAttackSent;
}

void StrategyManager::setBestStrategy()
{
	if (Options::Modules::USING_PREDICTION_MANAGER && !hasFinalStrategy && !Options::Modules::USING_RANDOM_STRATEGY)
	{
		std::vector<OpeningProbaPair> probas = PredictionManager::Instance().GetOpeningProbas(BWAPI::Broodwar->elapsedTime() + PRED_ADV_TIME);
		std::sort(probas.begin(), probas.end(), OpeningProbaPair::descending);

		float currTime = BWAPI::Broodwar->getFrameCount() / 24.f;
		if (probas[0].probability < .5f && currTime < 5 * 60)
		{
			// We are not sure of opponent's opening -> go for std play
			currentStrategy = Protoss4Gate;
			//BWAPI::Broodwar->printf("Too early, using %s", getCurrentStrategyName().c_str());
			return;
		}

		// We are sure of opponent's strategy or 
		// it's too late and we must decide about the counter
		if (enemyRace == BWAPI::Races::Protoss)
		{
			currentStrategy = getProtossCounter(probas);
		}
		else if (enemyRace == BWAPI::Races::Terran)
		{
			currentStrategy = getTerranCounter(probas);
		}
		else if (enemyRace == BWAPI::Races::Zerg)
		{
			currentStrategy = getZergCounter(probas);
		}
		else // Random race (and we have not seen a unit yet) - go for std play
		{
			// This should not really happen
			currentStrategy = Protoss4Gate;
		}

		//BWAPI::Broodwar->printf("Countering %s with proba: %Lg by %s!", probas[0].opening.c_str(), probas[0].probability * 100, getCurrentStrategyName().c_str());
		Logger::Instance() << "Countering " << probas[0].opening << " " << probas[0].probability << " by " << getCurrentStrategyName() << "\n";
		Logger::Instance() << "Second best is " << probas[1].opening << " " << probas[1].probability << "\n";
	}
}

std::string StrategyManager::getHighestProbaStrat(const std::vector<OpeningProbaPair>& probas) const
{
	std::string opening = probas[0].opening;
#if GABRIEL_LABELS
	if (opening == "unknown")
#else
	if (opening == "Unknown")
#endif
	{
		opening = probas[1].opening;
	}

	return opening;
}

const StrategyManager::PStrategies StrategyManager::getProtossCounter(const std::vector<OpeningProbaPair>& probas)
{
	std::string opening = getHighestProbaStrat(probas);
#if GABRIEL_LABELS
	if (opening == "two_gates")
	{
		return Protoss4Gate;
	}
	else if (opening == "fast_dt")
	{
		InformationManager::Instance().setInvisPredicted();
		return Protoss4Gate;
	}
	else if (opening == "templar")
	{
		return ProtossAir;
	}
	else if (opening == "speedzeal")
	{
		return ProtossRobo;
		// HT when implemented
	}
	else if (opening == "corsair")
	{
		return ProtossAntiAir;
	}
	else if (opening == "nony")
	{
		return ProtossRobo;
	}
	else if (opening == "reaver_drop")
	{
		return ProtossAntiAir;
	}
	else
	{
		return Protoss4Gate;
	}
#else
	if (opening == "FastDT")
	{
		InformationManager::Instance().setInvisPredicted();
		return Protoss4Gate;
	}
	else if (opening == "FastExpand")
	{
		return ProtossAir;
	}
	else if (opening == "FastLegs")
	{
		return ProtossRobo;
		// HT when implemented
	}
	else if (opening == "Carrier")
	{
		return ProtossAntiAir;
	}
	else if (opening == "FastObs")
	{
		return ProtossRobo;
	}
	else if (opening == "ReaverDrop")
	{
		return ProtossAntiAir;
	}
	else
	{
		return Protoss4Gate;
	}
#endif
}
const StrategyManager::PStrategies StrategyManager::getTerranCounter(const std::vector<OpeningProbaPair> &probas)
{
	std::string opening = getHighestProbaStrat(probas);
#if GABRIEL_LABELS
	if (opening == "bio")
	{
		return ProtossRobo;
	}
	else if (opening == "rax_fe")
	{
		return ProtossRobo;
	}
	else if (opening == "two_facto")
	{
		return ProtossAir;
	}
	else if (opening == "vultures")
	{
		InformationManager::Instance().setInvisPredicted();
		return ProtossAir;
	}
	else if (opening == "drop")
	{
		return ProtossAntiAir;
	}
	else
	{
		return Protoss4Gate;
	}
#else
	if (opening == "Bio")
	{
		return ProtossRobo;
	}
	else if (opening == "SiegeExpand")
	{
		return ProtossRobo;
	}
	else if (opening == "TwoFactory")
	{
		return ProtossAir;
	}
	else if (opening == "VultureHarass")
	{
		InformationManager::Instance().setInvisPredicted();
		return ProtossAir;
	}
	else if (opening == "FastDropship")
	{
		return ProtossAntiAir;
	}
	else
	{
		return Protoss4Gate;
	}
#endif
}
const StrategyManager::PStrategies StrategyManager::getZergCounter(const std::vector<OpeningProbaPair> &probas)
{
	std::string opening = getHighestProbaStrat(probas);
#if GABRIEL_LABELS
	if (opening == "fast_mutas")
	{
		return Protoss4Gate;
	}
	else if (opening == "mutas")
	{
		return ProtossAntiAir;
	}
	else if (opening == "lurkers")
	{
		InformationManager::Instance().setInvisPredicted();
		return ProtossAir;
	}
	else if (opening == "hydras")
	{
		return ProtossRobo;
	}
	else
	{
		return Protoss4Gate;
	}
#else
	if (opening == "TwoHatchMuta")
	{
		return Protoss4Gate;
	}
	else if (opening == "ThreeHatchMuta")
	{
		return ProtossAntiAir;
	}
	else if (opening == "Lurker")
	{
		InformationManager::Instance().setInvisPredicted();
		return ProtossAir;
	}
	else if (opening == "HydraRush" || opening == "HydraMass")
	{
		return ProtossRobo;
	}
	else
	{
		return Protoss4Gate;
	}
#endif
}

const MetaPairVector StrategyManager::getBuildOrderGoal(bool finalStrat)
{
	if (!hasFinalStrategy)
	{
		hasFinalStrategy = finalStrat;
	}
	if (!Options::Modules::USING_RANDOM_STRATEGY)
	{
		setBestStrategy();
	}

	if (BWAPI::Broodwar->self()->getRace() == BWAPI::Races::Protoss)
	{
		switch (getCurrentStrategy())
		{
		case Protoss4Gate:
			return getProtoss4Gate();
		case ProtossAir:
			return getProtossAir();
		case ProtossRobo:
			return getProtossRobo();
		case ProtossHT:
			return getProtossHT();
		case ProtossAntiAir:
			return getProtossAntiAir();
#if ORIGINAL_BO
		case ProtossZealotRush:
			return getProtossZealotRushBuildOrderGoal();
		case ProtossDragoons:
			return getProtossDragoonsBuildOrderGoal();
		case ProtossDarkTemplar:
			return getProtossDarkTemplarBuildOrderGoal();
#endif
		default:
			return getProtoss4Gate();
		}
	}
	return MetaPairVector();
}

void StrategyManager::HandleInvis(MetaPairVector &goal)
{
	if (InformationManager::Instance().enemyHasCloakedUnits() || InformationManager::Instance().getInvisPredicted())
	{

		if (InformationManager::Instance().getFirstTimeInvis())
		{
			BWAPI::Broodwar->printf("first time invis");
			goal.clear();
			goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Zealot, 2));
			goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dragoon, 2));
		}

		if (BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Photon_Cannon) < 1)
		{
			goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Photon_Cannon, 2));
		}
		else
		{
			goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Observer, 1));
		}
	}
}

void StrategyManager::HandleScouting(MetaPairVector &goal) const
{
	if (BWAPI::Broodwar->elapsedTime() < BUILD_FLY_SCOUT_TIME || hasFinalStrategy)
	{
		return;
	}

	int numCorsairs = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Corsair);

	// If we have at least one corsair we can scout with it
	if (numCorsairs == 0)
	{
		for (MetaPairVector::iterator it = goal.begin(); it != goal.end(); ++it)
		{
			// Are there corsairs already in the build order?
			if (it->first.unitType == BWAPI::UnitTypes::Protoss_Corsair)
			{
				++it->second;
				return;
			}
		}
		BWAPI::Broodwar->printf("adding corsair to scout");
		// There are no corsairs in the build order - add one corsair for scouting
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Corsair, 1));
	}
}

const bool StrategyManager::tryResearch(MetaPairVector &goal, const BWAPI::UpgradeType &upgrade) const
{
	int upgradeLevel = BWAPI::Broodwar->self()->getUpgradeLevel(upgrade);
	if (upgradeLevel < BWAPI::Broodwar->self()->getMaxUpgradeLevel(upgrade))
	{
		goal.push_back(MetaPair(upgrade, upgradeLevel + 1));
		return true;
	}
	return false;
}
const bool StrategyManager::tryResearch(MetaPairVector &goal, const BWAPI::TechType &tech) const
{
	bool techDone = false; // TODO how to find this out?
	if (!techDone)
	{
		goal.push_back(MetaPair(tech, 1));
		return true;
	}
	return false;
}

const MetaPairVector StrategyManager::getProtossAir()
{
	MetaPairVector goal;

	int numProbes =			BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Probe);
	int numNexusCompleted = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numCarriers =		BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Carrier);
	int numStargates =		BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Stargate);
	int numZealots =		BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Zealot);
	int numDragoons =		BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Dragoon);

	int carriersWanted = numCarriers ? numCarriers + 2 : 2;
	int stargateWanted = 1;
	int zealotWanted = numZealots + 2;
	int dragoonWanted = numDragoons + 2;
	int probesWanted = numProbes + numNexusCompleted * 3;
	int interceptorWanted = numCarriers * 6;
	int gatewayWanted = 2;
	int nexusWanted = 1;

	if (BWAPI::Broodwar->elapsedTime() > ADD_UNITS_TO_STRAT)
	{
		zealotWanted += 8;
		dragoonWanted += 6;
	}

	// If we have a stargate -> expand
	if (numStargates > 0)
	{
		nexusWanted = 2;
		stargateWanted = 2;
	}
	else if (numCarriers >= 4 && 
		numNexusCompleted > 1 &&
		numProbes >= numNexusCompleted * 18) // 2/3 saturated bases are optimal
	{
		nexusWanted = std::min(numNexusCompleted + 1, 4);
	}
	else
	{
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Nexus, 1));
	}
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Nexus, 2));

	if (numCarriers > 0)
	{
		goal.push_back(MetaPair(BWAPI::UpgradeTypes::Carrier_Capacity, 1));
		
		// For many carriers upgrades are worth the price
		if (numCarriers > 4)
		{
			int airArmor = BWAPI::Broodwar->self()->getUpgradeLevel(BWAPI::UpgradeTypes::Protoss_Air_Armor);
			int airWeapon = BWAPI::Broodwar->self()->getUpgradeLevel(BWAPI::UpgradeTypes::Protoss_Air_Weapons);
			if (airArmor < 3)
			{
				goal.push_back(MetaPair(BWAPI::UpgradeTypes::Protoss_Air_Weapons, airWeapon + 1));
				goal.push_back(MetaPair(BWAPI::UpgradeTypes::Protoss_Air_Armor, airArmor + 1));
			}
		}
	}

	if (Options::Modules::USING_ADVANCED_SCOUTING)
	{
		HandleScouting(goal);
	}

	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Zealot, zealotWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dragoon, dragoonWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Stargate, stargateWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Carrier, carriersWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Gateway, gatewayWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Probe, std::min(24 * numNexusCompleted + 2, probesWanted)));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Nexus, nexusWanted));

	HandleInvis(goal);

	return goal;
}

const MetaPairVector StrategyManager::getProtossAntiAir()
{
	MetaPairVector goal;

	int numProbes = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Probe);
	int numNexusCompleted = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numCarriers = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Carrier);
	int numScouts = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Scout);
	int numCorsairs = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Corsair);
	int numStargates = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Stargate);
	int numZealots = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Zealot);
	int numDragoons = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Dragoon);

	int carriersWanted = numCarriers ? numCarriers + 1 : 1;
	int scoutsWanted = 1;
	int corsairsWanted = numCorsairs + 1;
	int stargateWanted = 1;
	int zealotWanted = numZealots + 2;
	int dragoonWanted = numDragoons + 2;
	int probesWanted = numProbes + numNexusCompleted * 6;
	int interceptorWanted = numCarriers * 6;
	int gatewayWanted = 2;
	int nexusWanted = 1;

	if (BWAPI::Broodwar->elapsedTime() > ADD_UNITS_TO_STRAT)
	{
		zealotWanted += 8;
		dragoonWanted += 6;
	}

	// If we have a stargate -> expand
	if (numStargates > 0)
	{
		nexusWanted = 2;
		scoutsWanted = 2;
	}
	else if (numCarriers >= 4 &&
		numNexusCompleted > 1 &&
		numProbes >= numNexusCompleted * 18) // 2/3 saturated bases are optimal
	{
		nexusWanted = std::min(numNexusCompleted + 1, 4);
	}
	else
	{
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Nexus, 1));
	}
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Nexus, 2));

	if (numCarriers > 0)
	{
		goal.push_back(MetaPair(BWAPI::UpgradeTypes::Carrier_Capacity, 1));

		// For many carriers upgrades are worth the price
		if (numCarriers > 4)
		{
			int airArmor = BWAPI::Broodwar->self()->getUpgradeLevel(BWAPI::UpgradeTypes::Protoss_Air_Armor);
			int airWeapon = BWAPI::Broodwar->self()->getUpgradeLevel(BWAPI::UpgradeTypes::Protoss_Air_Weapons);
			if (airArmor < 3)
			{
				goal.push_back(MetaPair(BWAPI::UpgradeTypes::Protoss_Air_Weapons, airWeapon + 1));
				goal.push_back(MetaPair(BWAPI::UpgradeTypes::Protoss_Air_Armor, airArmor + 1));
			}
		}
	}

	if (Options::Modules::USING_ADVANCED_SCOUTING)
	{
		HandleScouting(goal);
	}

	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Zealot, zealotWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dragoon, dragoonWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Stargate, stargateWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Carrier, carriersWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Gateway, gatewayWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Probe, std::min(24 * numNexusCompleted + 2, probesWanted)));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Nexus, nexusWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Scout, scoutsWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Corsair, corsairsWanted));

	return goal;
}

const MetaPairVector StrategyManager::getProtossRobo()
{
	MetaPairVector goal;

	int numProbes = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Probe);
	int numNexusCompleted = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numZealots = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Zealot);
	int numDragoons = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Dragoon);
	int numReavers = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Reaver);

	int zealotWanted = numZealots + 4;
	int dragoonWanted = numDragoons + 3;
	int probesWanted = numProbes + numNexusCompleted * 3;
	int gatewayWanted = 2;
	int nexusWanted = 2;
	int reaversWanted = numReavers + 2;

	if (BWAPI::Broodwar->elapsedTime() > ADD_UNITS_TO_STRAT)
	{
		zealotWanted += 8;
		dragoonWanted += 6;
	}

	if (numReavers > 0)
	{
		tryResearch(goal, BWAPI::UpgradeTypes::Scarab_Damage);
		tryResearch(goal, BWAPI::UpgradeTypes::Leg_Enhancements);
		if (numReavers > 4)
		{
			tryResearch(goal, BWAPI::UpgradeTypes::Reaver_Capacity);
			tryResearch(goal, BWAPI::UpgradeTypes::Protoss_Ground_Armor);
			tryResearch(goal, BWAPI::UpgradeTypes::Protoss_Ground_Weapons);
		}
	}

	if (Options::Modules::USING_ADVANCED_SCOUTING)
	{
		HandleScouting(goal);
	}
	
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Reaver, reaversWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Zealot, zealotWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dragoon, dragoonWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Gateway, gatewayWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Probe, std::min(24 * numNexusCompleted + 2, probesWanted)));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Nexus, nexusWanted));

	HandleInvis(goal);

	return goal;
}

const MetaPairVector StrategyManager::getProtossHT()
{
	MetaPairVector goal;

	int numProbes = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Probe);
	int numNexusCompleted = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numArchivesCompleted = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Templar_Archives);
	int numZealots = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Zealot);
	int numDragoons = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Dragoon);
	int numHT = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_High_Templar);
	int numArchon = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Archon);

	int zealotWanted = numZealots + 2;
	int dragoonWanted = numDragoons + 1;
	int probesWanted = numProbes + numNexusCompleted * 6;
	int gatewayWanted = numArchivesCompleted ? 6 : 3;
	int nexusWanted = 1;
	//int htWanted = numHT ? numHT + 4 : 2;
	int archonWanted = 1;
	int archivesWanted = 1;

	// Research storm ASAP
	tryResearch(goal, BWAPI::TechTypes::Psionic_Storm);
	
	if (numArchivesCompleted)
	{
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Archon, archonWanted));
	}

	if (numHT > 0)
	{
		tryResearch(goal, BWAPI::UpgradeTypes::Singularity_Charge);
		tryResearch(goal, BWAPI::UpgradeTypes::Leg_Enhancements);
		if (numHT > 4)
		{
			tryResearch(goal, BWAPI::UpgradeTypes::Protoss_Ground_Armor);
			tryResearch(goal, BWAPI::UpgradeTypes::Protoss_Ground_Weapons);
		}
	}

	if (Options::Modules::USING_ADVANCED_SCOUTING)
	{
		HandleScouting(goal);
	}

	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Templar_Archives, archivesWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Zealot, zealotWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dragoon, dragoonWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Gateway, gatewayWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Probe, std::min(24 * numNexusCompleted + 2, probesWanted)));

	HandleInvis(goal);

	return goal;
}

const MetaPairVector StrategyManager::getProtoss4Gate()
{
	if (BWAPI::Broodwar->elapsedTime() > 16 * 60)
		return getProtossRobo();

	MetaPairVector goal;

	int numProbes = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Probe);
	int numNexusCompleted = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numZealots = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Zealot);
	int numDragoons = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Dragoon);
	int numGateway = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Gateway);

	int zealotWanted = numZealots + 4;
	int dragoonWanted = numDragoons + 2;
	int probesWanted = numProbes + numNexusCompleted * 4;
	int gatewayWanted = 1;
	int nexusWanted = 1;

	if (numGateway > 0)
	{
		gatewayWanted = 3;
	}
	if (BWAPI::Broodwar->elapsedTime() > ADD_UNITS_TO_STRAT)
	{
		gatewayWanted = 4;
		zealotWanted += 8;
		dragoonWanted += 6;
	}

	// Upgrades
	if (numGateway >= 4)
	{
		tryResearch(goal, BWAPI::UpgradeTypes::Leg_Enhancements);
		tryResearch(goal, BWAPI::UpgradeTypes::Singularity_Charge);
	}
	if (numZealots > 9 && numDragoons > 6)
	{
		tryResearch(goal, BWAPI::UpgradeTypes::Protoss_Ground_Armor);
		tryResearch(goal, BWAPI::UpgradeTypes::Protoss_Ground_Weapons);
	}

	if (Options::Modules::USING_ADVANCED_SCOUTING)
	{
		HandleScouting(goal);
	}

	// Finally create the goal
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Zealot, zealotWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dragoon, dragoonWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Gateway, gatewayWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Probe, std::min(24 * numNexusCompleted + 2, probesWanted)));

	HandleInvis(goal);

	return goal;
}

 const int StrategyManager::getCurrentStrategy() const
 {
	 return currentStrategy;
 }
 const std::string StrategyManager::getStrategyName(int strategy) const
 {
	 switch (strategy)
	 {
	 case Protoss4Gate:
		 return "Four gate";
	 case ProtossAir:
		 return "Air";
	 case ProtossAntiAir:
		 return "Anti Air";
	 case ProtossRobo:
		 return "Robo";
	 case ProtossHT:
		 return "High templars";
	 default:
		 return "Unknown strategy";
	 }
 }
 const std::string StrategyManager::getCurrentStrategyName() const
 {
	 return getStrategyName(getCurrentStrategy());
 }

 /* Scouting */
bool StrategyManager::ShouldScout(const BWAPI::Unit* scout) const
 {
	 // No reason for advanced scouting too early into the game, worker scout will take care of that
	 if (BWAPI::Broodwar->elapsedTime() < EARLIEST_ADV_SCOUT_TIME)
		 return false;

	 const int lastTime = InformationManager::Instance().getLastTimeScout();
	 
	 // TODO HACK Use this instead of last scout time
	 BWTA::BaseLocation * enemyBaseLocation = InformationManager::Instance().getMainBaseLocation(BWAPI::Broodwar->enemy()); 
	 
	 
	 int currentTime = BWAPI::Broodwar->elapsedTime();
	 if (BWAPI::Broodwar->elapsedTime() - lastTime < NEW_SCOUT_TIME_THOLD)
	 {
		 BWAPI::Broodwar->printf("Too early to scout again");
		 return false;
	 }

	 // Scout with ground units only early into the game
	 if ((scout->getType() == BWAPI::UnitTypes::Protoss_Zealot || // Hack use better way to determine if unit is ground
		 scout->getType() == BWAPI::UnitTypes::Protoss_Probe)
		 && BWAPI::Broodwar->elapsedTime() > GROUND_SCOUT_TIME_THOLD)
	 {
		 return false;
	 }

	 // Probability that enemy has build order for different opening than the one we currently expect
	 double openingChangedProba = PredictionManager::Instance().ComputeDistribChangeProba(BWAPI::Broodwar->elapsedTime());

	 if (openingChangedProba > OP_CHANGE_PROBA_THOLD)
	 {
		 //InformationManager::Instance().setLastTimeScout(BWAPI::Broodwar->elapsedTime());
		 return true;
	 }
	 
	 return false;
 }




#if ORIGINAL_BO
const bool StrategyManager::expandProtossZealotRush() const
{
	// if there is no place to expand to, we can't expand
	if (MapTools::Instance().getNextExpansion() == BWAPI::TilePositions::None)
	{
		return false;
	}

	int numNexus = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numZealots = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Zealot);
	int frame = BWAPI::Broodwar->getFrameCount();

	// if there are more than 10 idle workers, expand
	if (WorkerManager::Instance().getNumIdleWorkers() > 10)
	{
		return true;
	}

	// 2nd Nexus Conditions:
	//		We have 12 or more zealots
	//		It is past frame 7000
	if ((numNexus < 2) && (numZealots > 12 || frame > 9000))
	{
		return true;
	}

	// 3nd Nexus Conditions:
	//		We have 24 or more zealots
	//		It is past frame 12000
	if ((numNexus < 3) && (numZealots > 24 || frame > 15000))
	{
		return true;
	}

	if ((numNexus < 4) && (numZealots > 24 || frame > 21000))
	{
		return true;
	}

	if ((numNexus < 5) && (numZealots > 24 || frame > 26000))
	{
		return true;
	}

	if ((numNexus < 6) && (numZealots > 24 || frame > 30000))
	{
		return true;
	}

	return false;
}

const MetaPairVector StrategyManager::getProtossTmpStratBuildOrderGoal() const
{
	MetaPairVector goal;

	int numZealots = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Zealot);
	int numDragoons = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Dragoon);
	int numProbes = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Probe);
	int numNexusCompleted = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numNexusAll = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numCyber = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Cybernetics_Core);
	int numCannon = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Photon_Cannon);

	int dragoonsWanted = numDragoons;
	int probesWanted = numProbes + 4;


	dragoonsWanted = numDragoons + 6;


	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Probe, probesWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dragoon, dragoonsWanted));

	return goal;
}

const MetaPairVector StrategyManager::getProtossDragoonsBuildOrderGoal() const
{
	// the goal to return
	MetaPairVector goal;

	int numDragoons = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Dragoon);
	int numProbes = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Probe);
	int numNexusCompleted = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numNexusAll = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numCyber = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Cybernetics_Core);
	int numCannon = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Photon_Cannon);

	int dragoonsWanted = numDragoons > 0 ? numDragoons + 6 : 2;
	int gatewayWanted = 3;
	int probesWanted = numProbes + 6;

	HandleInvis(goal);

	if (numNexusAll >= 2 || numDragoons > 6 || BWAPI::Broodwar->getFrameCount() > 9000)
	{
		gatewayWanted = 6;
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Robotics_Facility, 1));
	}

	if (numNexusCompleted >= 3)
	{
		gatewayWanted = 8;
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Observer, 2));
	}

	if (numNexusAll > 1)
	{
		probesWanted = numProbes + 6;
	}

	if (expandProtossZealotRush())
	{
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Nexus, numNexusAll + 1));
	}

	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dragoon, dragoonsWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Gateway, gatewayWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Probe, std::min(90, probesWanted)));

	return goal;
}

const MetaPairVector StrategyManager::getProtossDarkTemplarBuildOrderGoal() const
{
	// the goal to return
	MetaPairVector goal;

	int numDarkTeplar = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Dark_Templar);
	int numDragoons = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Dragoon);
	int numProbes = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Probe);
	int numNexusCompleted = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numNexusAll = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numCyber = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Cybernetics_Core);
	int numCannon = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Photon_Cannon);

	int darkTemplarWanted = 0;
	int dragoonsWanted = numDragoons + 6;
	int gatewayWanted = 3;
	int probesWanted = numProbes + 6;

	if (InformationManager::Instance().enemyHasCloakedUnits())
	{

		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Robotics_Facility, 1));

		if (BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Robotics_Facility) > 0)
		{
			goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Observatory, 1));
		}
		if (BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Observatory) > 0)
		{
			goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Observer, 1));
		}
	}

	if (numNexusAll >= 2 || BWAPI::Broodwar->getFrameCount() > 9000)
	{
		gatewayWanted = 6;
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Robotics_Facility, 1));
	}

	if (numDragoons > 0)
	{
		goal.push_back(MetaPair(BWAPI::UpgradeTypes::Singularity_Charge, 1));
	}

	if (numNexusCompleted >= 3)
	{
		gatewayWanted = 8;
		dragoonsWanted = numDragoons + 6;
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Observer, 1));
	}

	if (numNexusAll > 1)
	{
		probesWanted = numProbes + 6;
	}

	if (expandProtossZealotRush())
	{
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Nexus, numNexusAll + 1));
	}

	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dragoon, dragoonsWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Gateway, gatewayWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dark_Templar, darkTemplarWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Probe, std::min(90, probesWanted)));

	return goal;
}

const MetaPairVector StrategyManager::getProtossZealotRushBuildOrderGoal() const
{
	// the goal to return
	MetaPairVector goal;

	int numZealots = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Zealot);
	int numDragoons = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Dragoon);
	int numProbes = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Probe);
	int numNexusCompleted = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numNexusAll = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Nexus);
	int numCyber = BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Cybernetics_Core);
	int numCannon = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Protoss_Photon_Cannon);

	int zealotsWanted = numZealots + 8;
	int dragoonsWanted = numDragoons;
	int gatewayWanted = 3;
	int probesWanted = numProbes + 4;

	if (InformationManager::Instance().enemyHasCloakedUnits())
	{
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Robotics_Facility, 1));

		if (BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Robotics_Facility) > 0)
		{
			goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Observatory, 1));
		}
		if (BWAPI::Broodwar->self()->completedUnitCount(BWAPI::UnitTypes::Protoss_Observatory) > 0)
		{
			goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Observer, 1));
		}
	}

	if (numNexusAll >= 2 || BWAPI::Broodwar->getFrameCount() > 9000)
	{
		gatewayWanted = 6;
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Assimilator, 1));
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Cybernetics_Core, 1));
	}

	if (numCyber > 0)
	{
		dragoonsWanted = numDragoons + 2;
		goal.push_back(MetaPair(BWAPI::UpgradeTypes::Singularity_Charge, 1));
	}

	if (numNexusCompleted >= 3)
	{
		gatewayWanted = 8;
		dragoonsWanted = numDragoons + 6;
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Observer, 1));
	}

	if (numNexusAll > 1)
	{
		probesWanted = numProbes + 6;
	}

	if (expandProtossZealotRush())
	{
		goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Nexus, numNexusAll + 1));
	}

	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Dragoon, dragoonsWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Zealot, zealotsWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Gateway, gatewayWanted));
	goal.push_back(MetaPair(BWAPI::UnitTypes::Protoss_Probe, std::min(90, probesWanted)));

	return goal;
}

const MetaPairVector StrategyManager::getTerranBuildOrderGoal() const
{
	// the goal to return
	std::vector< std::pair<MetaType, UnitCountType> > goal;

	int numMarines = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Terran_Marine);
	int numMedics = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Terran_Medic);
	int numWraith = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Terran_Wraith);

	int marinesWanted = numMarines + 12;
	int medicsWanted = numMedics + 2;
	int wraithsWanted = numWraith + 4;

	goal.push_back(std::pair<MetaType, int>(BWAPI::UnitTypes::Terran_Marine, marinesWanted));

	return (const std::vector< std::pair<MetaType, UnitCountType> >)goal;
}

const MetaPairVector StrategyManager::getZergBuildOrderGoal() const
{
	// the goal to return
	std::vector< std::pair<MetaType, UnitCountType> > goal;

	int numMutas = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Zerg_Mutalisk);
	int numHydras = BWAPI::Broodwar->self()->allUnitCount(BWAPI::UnitTypes::Zerg_Hydralisk);

	int mutasWanted = numMutas + 6;
	int hydrasWanted = numHydras + 6;

	goal.push_back(std::pair<MetaType, int>(BWAPI::UnitTypes::Zerg_Zergling, 4));
	//goal.push_back(std::pair<MetaType, int>(BWAPI::TechTypes::Stim_Packs,	1));

	//goal.push_back(std::pair<MetaType, int>(BWAPI::UnitTypes::Terran_Medic,		medicsWanted));

	return (const std::vector< std::pair<MetaType, UnitCountType> >)goal;
}
#endif