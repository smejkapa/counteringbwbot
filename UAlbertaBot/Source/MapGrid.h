#pragma once

#include <Common.h>
#include "micromanagement/MicroManager.h"
#include <math.h>

/** Represents a single cell of given dimensions on the game map. */
class GridCell
{
public:
	/** All possible danger levels with values used for pathfinding */
	enum DangerLevel
	{
		Safe = 0,
		NearDanger = 1,
		AroundWarPath = 7,
		BaseDefense = 8,
		WarPath = 9,
		ChokePoint = 10
	};

	int					timeLastVisited, timeLastOpponentSeen;
	UnitVector			ourUnits;
	UnitVector			oppUnits;
	BWAPI::Position		center;
	/** Current danger level of this cell */
	DangerLevel	        danger;

	/* Default danger level for a cell is Safe */
	GridCell() :		timeLastVisited(0), timeLastOpponentSeen(0), danger(GridCell::Safe)  {}

	/** Returns current danger level as a string */
	std::string DangerToString() const;
	/** Returns color according to danger level - used for debugging */
	BWAPI::Color GetColor() const;
	/** Returns cost of passing through this tile according to danger level */
	int GetCost() const;
};


/** Wrapper on the GridCell class able to determine distance from given base and capable of sorting. */
class BaseDefenseCell
{
public:
	BaseDefenseCell(GridCell *cell, const BWAPI::Position &myBase) 
		: cell(cell)
	{
		distance = myBase.getDistance(cell->center);
	}

	/** The GridCell to wrap */
	GridCell* cell;
	/** Euclidean distance from given base */
	double distance;
	/** Operators for sorting */
	friend inline bool operator< (const BaseDefenseCell& lhs, const BaseDefenseCell& rhs);
	friend inline bool operator>(const BaseDefenseCell& lhs, const BaseDefenseCell& rhs);
	friend inline bool operator<=(const BaseDefenseCell& lhs, const BaseDefenseCell& rhs);
	friend inline bool operator>=(const BaseDefenseCell& lhs, const BaseDefenseCell& rhs);
	friend inline bool operator==(const BaseDefenseCell& lhs, const BaseDefenseCell& rhs);
	friend inline bool operator!=(const BaseDefenseCell& lhs, const BaseDefenseCell& rhs);
};

/** Operators for BaseDefenseCell sorting */
inline bool operator< (const BaseDefenseCell& lhs, const BaseDefenseCell& rhs) { return lhs.distance < rhs.distance; }
inline bool operator>(const BaseDefenseCell& lhs, const BaseDefenseCell& rhs){ return  operator< (rhs, lhs); }
inline bool operator<=(const BaseDefenseCell& lhs, const BaseDefenseCell& rhs){ return !operator> (lhs, rhs); }
inline bool operator>=(const BaseDefenseCell& lhs, const BaseDefenseCell& rhs){ return !operator< (lhs, rhs); }
inline bool operator==(const BaseDefenseCell& lhs, const BaseDefenseCell& rhs){ return (lhs.cell == rhs.cell); }
inline bool operator!=(const BaseDefenseCell& lhs, const BaseDefenseCell& rhs){ return !operator==(lhs, rhs); }

/** Represents cell coordinates (column, row) in a single structure for easier use */
struct CellCoords
{
	CellCoords(int row, int col) : row(col), col(row) {}
	int row;
	int col;
};


class MapGrid {

	MapGrid();
	MapGrid(int mapWidth, int mapHeight, int cellSize);

	int							cellSize;
	int							mapWidth, mapHeight;
	int							rows, cols;
	int							lastUpdated;

	bool						contains(UnitVector & units, BWAPI::Unit * unit);

	std::vector< GridCell >		cells;

	void						calculateCellCenters();

	void						clearGrid();
	BWAPI::Position				getCellCenter(int x, int y);

	BWAPI::Position				naturalExpansion;

	/** Flag indicating whether danger levels were calculated already */
	bool						dangerLevelCalculated;

	/** Sets danger levels of cells adjacent to given cell to given value */
	void SetAdjecentTo(GridCell* cell, GridCell::DangerLevel dangerLevel);
	/** Returns a cell on given row and column */
	inline GridCell & getCellByIndex(int r, int c)		{ return cells[r*cols + c]; }
	/** Returns a cell given unit is in */
	inline GridCell & getCell(BWAPI::Unit * unit)		{ return getCell(unit->getPosition()); }
	/** Returns coordinates (row, column) of given cell */
	inline CellCoords GetCellCoords(const GridCell& cell) const { return CellCoords(cell.center.x() / cellSize, cell.center.y() / cellSize); }
	
public:

	// yay for singletons!
	static MapGrid &	Instance();

	void				update();
	void				GetUnits(UnitVector & units, BWAPI::Position center, int radius, bool ourUnits, bool oppUnits);
	BWAPI::Position		getLeastExplored();
	BWAPI::Position		getNaturalExpansion();

	/** Returns a cell for given position */
	inline GridCell & getCell(BWAPI::Position pos)		{ return getCellByIndex(pos.y() / cellSize, pos.x() / cellSize); }

	/** Returns cells adjacent to given cell. Cells are adjacent even if they share only a corner */
	const std::vector<GridCell*> GetAdjacentCells(const GridCell& cell);

	/** Assigns a danger level to all cells. Once successfully computed does not recompute again.
	*	The danger levels cannot change throughout the game. */
	void CalculateDangerMap();

	/** Returns size of cell defined at the beginning of the game. */
	inline int GetCellSize() const { return cellSize; }
};

/** Wrapper class for a GridCell used for A* pathfinding. */
class PathCell
{
public:
	PathCell(GridCell *cell, const BWAPI::Position &enemyBase)
		: cell(cell),
		gScore(0)
	{
		distance = static_cast<int>(floor(enemyBase.getDistance(cell->center) / MapGrid::Instance().GetCellSize()));
	}

	/** A GridCell we are wrapping */
	GridCell* cell;

	/** Standard f() A* function combining previous path with heuristic approximation */
	inline int f() const { return g() + h(); }
	/** Standard h() A* function. Heuristics approximation of current distance to goal using Euclidean distance */
	inline int h() const { return distance; }
	/** Standard g() A* function. Previous already found path length */
	inline int g() const { return gScore; }
	/** Used to set value of the g() function */
	inline void setG(const int gValue) { gScore = gValue; }
	/** Returns cost of going through this cell (danger value) */
	inline int GetCost() const { return cell->GetCost(); }

	/** Returns cells adjacent to this cell. Cells are adjacent even if they share only a corner. */
	std::vector<PathCell> GetAdjacent(const BWAPI::Position &enemyBase) const;

	/** Friended operators */
	friend inline bool operator< (const PathCell& lhs, const PathCell& rhs);
	friend inline bool operator>(const PathCell& lhs, const PathCell& rhs);
	friend inline bool operator<=(const PathCell& lhs, const PathCell& rhs);
	friend inline bool operator>=(const PathCell& lhs, const PathCell& rhs);
	friend inline bool operator==(const PathCell& lhs, const PathCell& rhs);
	friend inline bool operator!=(const PathCell& lhs, const PathCell& rhs);

private:
	/** Heuristic distance to goal */
	int distance;
	/** Actual distance from start (with dangers included) */
	int gScore;
};

/** Operators for PathCell */
inline bool operator< (const PathCell& lhs, const PathCell& rhs) { return lhs.f() < rhs.f(); }
inline bool operator>(const PathCell& lhs, const PathCell& rhs){ return  operator< (rhs, lhs); }
inline bool operator<=(const PathCell& lhs, const PathCell& rhs){ return !operator> (lhs, rhs); }
inline bool operator>=(const PathCell& lhs, const PathCell& rhs){ return !operator< (lhs, rhs); }
inline bool operator==(const PathCell& lhs, const PathCell& rhs){ return (lhs.cell == rhs.cell); }
inline bool operator!=(const PathCell& lhs, const PathCell& rhs){ return !operator==(lhs, rhs); }
