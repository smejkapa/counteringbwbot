#pragma once

#include "BWAPI.h"
#include "boost/archive/text_iarchive.hpp"
#include "serialized_tables.h"
#include "CSingleton.h"
#include "Options.h"
#include <string>
#include <set>

/***
***********************
*openingsProba content*
***********************
|=============|
|Ben's labels:| (extracted with rules)
|=============|

Terran openings, in order (in the vector):
- Bio
- TwoFactory
- VultureHarass
- SiegeExpand
- Standard
- FastDropship
- Unknown

Protoss openings, in order (in the vector):
- FastLegs
- FastDT
- FastObs
- ReaverDrop
- Carrier
- FastExpand
- Unknown

Zerg openings, in order (in the vector):
- TwoHatchMuta
- ThreeHatchMuta
- HydraRush
- Standard
- HydraMass
- Lurker
- Unknown

|=================|
|Gabriel's labels:| (extracted by clustering)
|=================|

Terran openings, in order (in the vector):
- bio
- rax_fe
- two_facto
- vultures
- drop
- unknown

Protoss openings, in order (in the vector):
- two_gates
- fast_dt
- templar
- speedzeal
- corsair
- nony
- reaver_drop
- unknown

Zerg openings, in order (in the vector):
- fast_mutas
- mutas
- lurkers
- hydras
- unknown
*/

/** Minimal probability used - to avoid using 0 */
#define MIN_PROB 0.00000000000000000001;

/** Represents a simple pair of opening string and probability assigned to it. Can be sorted. */
struct OpeningProbaPair
{
	OpeningProbaPair(const std::string& name, long double proba)
		: opening(name), probability(proba) {}
	
	std::string opening;
	long double probability;

	/** Predicates for sorting */
	bool operator< (const OpeningProbaPair& str) const
	{
		return (probability < str.probability);
	}
	static bool descending(OpeningProbaPair a, OpeningProbaPair b)
	{
		return b < a;
	}
};

/** Represents a simple par of build tree index and probability of this build tree. */
struct BuildTreeProbaPair
{
	BuildTreeProbaPair(const size_t index, long double proba)
		: index(index), probability(proba) {}
	
	/** Index into the serialized table of learned build trees */
	size_t index;

	/** Probability that this build tree is the one our opponent has at current time */
	long double probability;
};

/** Implements the probabilistic model
*	Implemented as a singleton class
*/
class PredictionManager : public CSingleton<PredictionManager>
{
private:
	friend class CSingleton < PredictionManager > ;

	PredictionManager();
	~PredictionManager();

	/** This is the time our dataset ends. We cannot predict after it. */
	static const int LEARNED_TIME_LIMIT = 1080; // 18 minutes

	/** Serialized tables representing of our dataset */
	serialized_tables serializedTables;

	/** Prior probability of the opening distribution */
	openings_knowing_player op_prior;
	
	/** Tables to handle random race and switch later */
	serialized_tables TvP; // Temporary table to handle random race
	serialized_tables ZvP; // Temporary table to handle random race
	openings_knowing_player op_prior_TvP; // Temporary table to handle random race
	openings_knowing_player op_prior_ZvP; // Temporary table to handle random race

	/** Our current prediction. Probability distribution over openings. 
	*	The order corresponds to the order of openings in the dataset. */
	std::vector<long double> openingsProbas;

	/** Indicates whether we already loaded the correct table to memory */
	bool tableLoaded;
	/** Indicates whether we saw first overlord when playing against a Zerg player */
	bool notFirstOverlord;

	/** Loads dataset from given file to memory. */
	void LoadTable(const std::string& tablePath);

	/** Initializes prior probability distribution over openings */
	void InitializePrior(const std::string& filename, const BWAPI::Race& race, bool random);
	/** Swap the loaded probability table if we play against random race and we just found out the actual race. */
	void SwapRaceOnFound(BWAPI::Race r);
	/** Helper function returning time value in bounds of our learned dataset to be able to safely index to arrays */
	int format_max_time(int t);

	/** Our current set of observations */
	std::set<int> buildingsTypesSeen;

	/** Set of unit types we already saw */
	std::set<BWAPI::UnitType> _alreadySawUnitTypes;
	inline bool alreadySaw(BWAPI::UnitType ut);
	
	/** Adds given building or unit to our set of observations - if not already present etc. */
	bool insertBuilding(BWAPI::Unit* u);
	bool insertBuilding(BWAPI::UnitType ut);

	/** Computes the probability distribution over openings in given time with given observations */
	std::vector<long double> computeVecDistribOpenings(int time, const std::set<int>& observations);
	/** Computes the probability distribution over openings in given time with given observations
	*	computePrior indicates whether a prior will be recomputed during this inference*/
	std::vector<long double> computeDistribOpenings(int time, const std::set<int>& observations, bool computePrior);
	std::vector<long double> computeDistribOpenings(int time, const std::set<int>& observations);
	
	/**
	* Tests if the given build tree (X) value
	* is compatible with observations (what have been seen)
	* {X ^ observed} covers all observed if X is possible
	* so X is impossible if {observed \ {X ^ observed}} != {}
	* => X is compatible with observations if it covers them fully
	*/
	bool testBuildTreePossible(int indBuildTree, const std::set<int>& setObs);

	/** Used to debug the prediction manager by showing unknown buildings etc. */
	std::set<int> learnedBuildings;
	void getLearnedBuildings();
	void logAllBuildTrees();

	/**
	* Computes P(BT|T) * P(T) from the Bayesian network
	* P(BT|T) = alpha * sum_OP( P(T|BT, OP) * P(BT|OP) * P(OP) )
	* Note that P(OP) is P(OP|T)
	*/
	std::vector<BuildTreeProbaPair> GetP_BT_T(int time, const std::vector<size_t>& buildTrees);

	// Vector of missing units from Gabriel's serialized tables
	std::set<int> missingUnitTypes;
	
	const std::vector<OpeningProbaPair> GetOpeningProbas(const std::set<int>& observations, int time = BWAPI::Broodwar->elapsedTime());
public:

	/** Computes the probability distribution of openings at given time */
	const std::vector<OpeningProbaPair> GetOpeningProbas(int time = BWAPI::Broodwar->elapsedTime());	

	/**
	* Computes the probability our distribution changes (the most probable opening changes)
	* based on the probability of our opponent's current build tree (not only the observations, but what he probably actually has)
	*/
	double ComputeDistribChangeProba(int time);

	/** Event handlers */
	void onStart();
	void onUnitDestroy(BWAPI::Unit* u);
	void onUnitShow(BWAPI::Unit* u);
	void onUnitHide(BWAPI::Unit* u);
	void onFrame();
#ifdef PREDICTION_MANAGER_DEBUG
	void drawPredictionInformation();
#endif
};