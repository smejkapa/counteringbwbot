#include "AdvancedScoutManager.h"
#include "StrategyManager.h"

using namespace AdvScout;

void AdvancedScoutManager::update(const std::set<BWAPI::Unit *> & scoutUnits)
{
	try
	{
		if (scoutUnits.size() == 1)
		{
			// Currently supporting only one scouting unit
			BWAPI::Unit * scoutUnit = *scoutUnits.begin();
			if (scoutUnit != scout.GetUnit())
			{
				scout = ScoutUnit(scoutUnit);
			}
		}
	
		moveScouts();
	}
	catch (...)
	{
		Logger::Instance().log("Fatal error in advanced scout manager");
	}
}

void AdvancedScoutManager::moveScouts()
{
	// Move scouts only if already scouting or if we are not scouting but StrategyManager says we should
	if (!scout.IsValid())
	{
		return;
	}

	BWTA::BaseLocation * enemyBaseLocation = InformationManager::Instance().getMainBaseLocation(BWAPI::Broodwar->enemy());
	if (!enemyBaseLocation)
		return;

	// New scout - does not have a path yet - calculate a path for it
	if (scout.GetState() == ScoutUnit::Idle && StrategyManager::Instance().ShouldScout(scout.GetUnit()))
	{
		MapGrid::Instance().CalculateDangerMap();
		scout.SetPath(FindSafePath(scout, enemyBaseLocation));
		scout.StartMission();
	}

	scout.Update();
}

// A* path finding from current position to enemy base polygon
ScoutPath AdvancedScoutManager::FindSafePath(const ScoutUnit& scout, const BWTA::BaseLocation *enemyBase) const
{
	BWAPI::Broodwar->printf("Finding safe path");
	// For ground scouts just create strait path to enemy base
	if (scout.GetUnit()->getType() != BWAPI::UnitTypes::Protoss_Corsair
		&& scout.GetUnit()->getType() != BWAPI::UnitTypes::Protoss_Observer
		&& scout.GetUnit()->getType() != BWAPI::UnitTypes::Protoss_Scout)
	{
		ScoutPath path;
		path.push_back(scout.GetUnit()->getPosition());
		path.push_back(enemyBase->getPosition());
		return path;
	}

	PathCell &start = PathCell(&MapGrid::Instance().getCell(scout.GetUnit()->getPosition()), enemyBase->getPosition());
	start.setG(start.GetCost());

	// From current position go to safe position without enemies from where the scouting mission will begin

	// Perform A* path finding
	typedef std::map<GridCell*, int> GValues;
	Opened opened;
	opened.push_back(start);
	GValues gValues;
	gValues.insert(std::make_pair(start.cell, start.g()));
	std::set<GridCell*> closed;
	Preceding preceding;

	ScoutPath path;

	while (!opened.empty())
	{
		PathCell current = *opened.begin();
		opened.erase(opened.begin());

		// We're inside enemy base -> path found, let's just generate it
		if (enemyBase->getRegion()->getPolygon().isInside(current.cell->center))
		{
			path = GeneratePath(current, preceding);
			break;
		}

		// Close current node
		closed.insert(current.cell);

		// Expand current node
		std::vector<PathCell> adjacent = current.GetAdjacent(enemyBase->getPosition());
		for (std::vector<PathCell>::iterator i = adjacent.begin(); i != adjacent.end(); ++i)
		{
			// Don't reopen already closed cells
			if (closed.find(i->cell) != closed.end())
				continue;
			GValues::iterator oldG = gValues.find(i->cell);
			int newG = i->GetCost() + gValues.find(current.cell)->second;

			// New cell
			if (oldG == gValues.end())
			{
				gValues.insert(std::make_pair(i->cell, newG));
				i->setG(newG);
				opened.push_back(*i);
				preceding.insert(std::make_pair(i->cell, current.cell));
			}
			else if (newG < oldG->second) // Old cell but better path
			{
				// Update gValue
				oldG->second = newG;
				i->setG(newG);
				// Also update the preceding table
				Preceding::iterator previous = preceding.find(i->cell);
				if (previous != preceding.end()) // This must be true because we already found g
				{
					previous->second = current.cell;
				}
			}
		}

		// Sort by f()
		opened.sort();
	}

	return path;
}
ScoutPath AdvancedScoutManager::GeneratePath(const PathCell &goal, const Preceding &preceding) const
{
	BWAPI::Broodwar->printf("Generating path");
	ScoutPath path;
	path.push_front(goal.cell->center);
	GridCell * current = goal.cell;

	for (;;)
	{
		Preceding::const_iterator previous = preceding.find(current);
		if (previous == preceding.end())
			return path;
		path.push_front(previous->second->center);
		current = previous->second;
	}
}

BWAPI::Unit* AdvancedScoutManager::FindBestScout(const std::set<BWAPI::Unit*>& units)
{
	BWAPI::Unit* bestScout = NULL;
	for (std::set<BWAPI::Unit*>::const_iterator it = units.begin(); it != units.end(); ++it)
	{
		// Try to find the best scout from available units
		if (bestScout == NULL || isBetterScout(*it, bestScout))
		{
			bestScout = *it;
		}
	}

	return bestScout;
}

const AdvancedScoutManager::ScoutType AdvancedScoutManager::getScoutType(const BWAPI::Unit* scout)
{
	if (scout->getType() == BWAPI::UnitTypes::Protoss_Observer)
		return Observer;
	if (scout->getType() == BWAPI::UnitTypes::Protoss_Corsair)
		return Corsair;
	if (scout->getType() == BWAPI::UnitTypes::Protoss_Scout)
		return Scout;
	if (scout->getType() == BWAPI::UnitTypes::Protoss_Zealot)
		return Zealot;
	if (scout->getType() == BWAPI::UnitTypes::Protoss_Probe)
		return Probe;
	if (scout->getType().airWeapon() != BWAPI::WeaponTypes::None)
		return Air;
	return Land;
}

const bool AdvancedScoutManager::isBetterScout(const BWAPI::Unit * newScout, const BWAPI::Unit * oldScout)
{
	// Check if the type is superior
	if (getScoutType(oldScout) > getScoutType(newScout))
	{
		return true;
	}
	// If the type is same -> we want a scout with enough HP so it will survive a few shots
	else if (getScoutType(oldScout) == getScoutType(newScout))
	{
		return newScout->getHitPoints() > oldScout->getHitPoints();
	}
	return false;
}



//////////////////
/// SCOUT UNIT ///
//////////////////
void ScoutUnit::Update()
{
#ifdef ADV_SCOUT_DEBUG
	DrawDebug();
#endif
	if (lastUpdateTime != 0 && lastUpdateTime < BWAPI::Broodwar->elapsedTime())
	{
		age += BWAPI::Broodwar->elapsedTime() - lastUpdateTime;
	}
	lastUpdateTime = BWAPI::Broodwar->elapsedTime();

	switch (state)
	{
	case ScoutUnit::Idle: // Do nothing
		return;
	case ScoutUnit::GoingToScout: // Follow path to enemy base
	{
		if (currentNode->getApproxDistance(unit->getPosition()) < 100)
		{
			++currentNode;
			if (currentNode == path.end())
			{
				state = InEnemyBase;
				break;
			}
		}
		unit->move(*currentNode);
		break;
	}
	case ScoutUnit::InEnemyBase: // Explore the enemy base polygon
	{
		BWTA::BaseLocation * enemyBaseLocation = InformationManager::Instance().getMainBaseLocation(BWAPI::Broodwar->enemy());
		if (enemyBaseLocation->getPosition().getApproxDistance(unit->getPosition()) < 200)
		{
			state = CommingFromMission;
			currentNode = path.begin();
			currentBackNode = path.rbegin();
		}
		else
		{
			unit->move(enemyBaseLocation->getPosition());
		}
		break;
	}
	case ScoutUnit::CommingFromMission: // Go home following the same path
	{
		if (currentBackNode->getApproxDistance(unit->getPosition()) < 64)
		{
			++currentBackNode;
			if (currentBackNode == path.rend())
			{
				state = Idle;
				break;
			}
			unit->move(*currentBackNode);
		}
		break;
	}
	default:
		break;
	}
}

#ifdef ADV_SCOUT_DEBUG
void ScoutUnit::DrawDebug() const
{
	std::string state_string;
	switch (state)
	{
	case ScoutUnit::Idle:
		state_string = "Idle";
		break;
	case ScoutUnit::GoingToScout:
		state_string = "GoingToScout";
		break;
	case ScoutUnit::InEnemyBase:
		state_string = "InEnemyBase";
		break;
	case ScoutUnit::CommingFromMission:
		state_string = "CommingFromMission";
		break;
	default:
		state_string = "WTF";
		break;
	}

	// Print current state to screen
	BWAPI::Broodwar->printf("current state: %s", state_string.c_str());

	// Indicate current scout by drawing a circle on it
	BWAPI::Broodwar->drawCircleMap(unit->getPosition().x(), unit->getPosition().y(), 5, BWAPI::Colors::Red, true);

	// If we follow a path, show in on screen
	if (path.size() <= 0)
		return;

	ScoutPath::const_iterator next = path.begin();
	++next;
	for (ScoutPath::const_iterator previous = path.begin(); next != path.end(); ++previous, ++next)
	{
		BWAPI::Broodwar->drawLineMap(previous->x(), previous->y(), next->x(), next->y(), BWAPI::Colors::Green);
	}
}
#endif
