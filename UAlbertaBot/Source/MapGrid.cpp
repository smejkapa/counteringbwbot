#include "Common.h"
#include "MapGrid.h"

using namespace std;

MapGrid & MapGrid::Instance() 
{
	static MapGrid instance(BWAPI::Broodwar->mapWidth()*32, BWAPI::Broodwar->mapHeight()*32, Options::Tools::MAP_GRID_SIZE);
	return instance;
}

MapGrid::MapGrid() {}
	
MapGrid::MapGrid(int mapWidth, int mapHeight, int cellSize) 
	: mapWidth(mapWidth)
	, mapHeight(mapHeight)
	, cellSize(cellSize)
	, cols((mapWidth + cellSize - 1) / cellSize)
	, rows((mapHeight + cellSize - 1) / cellSize)
	, cells(rows * cols)
	, lastUpdated(0)
	, dangerLevelCalculated(false)
{
	calculateCellCenters();
}

BWAPI::Position MapGrid::getNaturalExpansion() 
{
	return naturalExpansion;
}

BWAPI::Position MapGrid::getLeastExplored() 
{
	int minSeen = 1000000;
	double minSeenDist = 100000;
	int leastRow(0), leastCol(0);

	for (int r=0; r<rows; ++r)
	{
		for (int c=0; c<cols; ++c)
		{
			// get the center of this cell
			BWAPI::Position cellCenter = getCellCenter(r,c);

			// don't worry about places that aren't connected to our start locatin
			if (!BWTA::isConnected(BWAPI::TilePosition(cellCenter), BWAPI::Broodwar->self()->getStartLocation()))
			{
				continue;
			}

			BWAPI::Position home(BWAPI::Broodwar->self()->getStartLocation());
			double dist = home.getDistance(getCellByIndex(r, c).center);
			if ((getCellByIndex(r, c).timeLastVisited < minSeen) || ((getCellByIndex(r, c).timeLastVisited == minSeen) && (dist < minSeenDist)))
			{
				leastRow = r;
				leastCol = c;
				minSeen = getCellByIndex(r, c).timeLastVisited;
				minSeenDist = dist;
			}
		}
	}

	return getCellCenter(leastRow, leastCol);
}

void MapGrid::calculateCellCenters()
{
	for (int r=0; r < rows; ++r)
	{
		for (int c=0; c < cols; ++c)
		{
			GridCell & cell = getCellByIndex(r,c);

			int centerX = (c * cellSize) + (cellSize / 2);
			int centerY = (r * cellSize) + (cellSize / 2);

			// if the x position goes past the end of the map
			if (centerX > mapWidth)
			{
				// when did the last cell start
				int lastCellStart		= c * cellSize;

				// how wide did we go
				int tooWide				= mapWidth - lastCellStart;
				
				// go half the distance between the last start and how wide we were
				centerX = lastCellStart + (tooWide / 2);
			}
			else if (centerX == mapWidth)
			{
				centerX -= 50;
			}

			if (centerY > mapHeight)
			{
				// when did the last cell start
				int lastCellStart		= r * cellSize;

				// how wide did we go
				int tooHigh				= mapHeight - lastCellStart;
				
				// go half the distance between the last start and how wide we were
				centerY = lastCellStart + (tooHigh / 2);
			}
			else if (centerY == mapHeight)
			{
				centerY -= 50;
			}

			cell.center = BWAPI::Position(centerX, centerY);
			assert(cell.center.isValid());
		}
	}
}

BWAPI::Position MapGrid::getCellCenter(int row, int col)
{
	return getCellByIndex(row, col).center;
}

// clear the vectors in the grid
void MapGrid::clearGrid() { 

	for (size_t i(0); i<cells.size(); ++i) 
	{
		cells[i].ourUnits.clear();
		cells[i].oppUnits.clear();
	}
}

// populate the grid with units
void MapGrid::update() 
{
	for (int i=0; i<cols; i++) 
	{
		if (Options::Debug::DRAW_UALBERTABOT_DEBUG) BWAPI::Broodwar->drawLineMap(i*cellSize, 0, i*cellSize, mapHeight, BWAPI::Colors::Blue);
	}

	for (int j=0; j<rows; j++) 
	{
		if (Options::Debug::DRAW_UALBERTABOT_DEBUG) BWAPI::Broodwar->drawLineMap(0, j*cellSize, mapWidth, j*cellSize, BWAPI::Colors::Blue);
	}

	for (int r=0; r < rows; ++r)
	{
		for (int c=0; c < cols; ++c)
		{
			GridCell & cell = getCellByIndex(r,c);
			
			if (Options::Debug::DRAW_UALBERTABOT_DEBUG)
			{
				BWAPI::Broodwar->drawBox(BWAPI::CoordinateType::Map,
					cell.center.x() - 10, cell.center.y() + 21,
					cell.center.x() + 10, cell.center.y() + 41,
					cell.GetColor(), true);
				BWAPI::Broodwar->drawTextMap(cell.center.x(), cell.center.y(), "Last Seen %d", cell.timeLastVisited);
				BWAPI::Broodwar->drawTextMap(cell.center.x(), cell.center.y() + 10, "Row/Col (%d, %d)", r, c);
				BWAPI::Broodwar->drawTextMap(cell.center.x(), cell.center.y() + 20, "Danger: %s", cell.DangerToString().c_str());
			}
		}
	}

	// Remove all units from all cells
	clearGrid();

	//BWAPI::Broodwar->printf("MapGrid info: WH(%d, %d)  CS(%d)  RC(%d, %d)  C(%d)", mapWidth, mapHeight, cellSize, rows, cols, cells.size());

	// add our units to the appropriate cell
	BOOST_FOREACH(BWAPI::Unit * unit, BWAPI::Broodwar->self()->getUnits()) 
	{
		getCell(unit).ourUnits.push_back(unit);
		getCell(unit).timeLastVisited = BWAPI::Broodwar->getFrameCount();
	}

	// add enemy units to the appropriate cell
	BOOST_FOREACH(BWAPI::Unit * unit, BWAPI::Broodwar->enemy()->getUnits()) 
	{
		if (unit->getHitPoints() > 0) 
		{
			getCell(unit).oppUnits.push_back(unit);
			getCell(unit).timeLastOpponentSeen = BWAPI::Broodwar->getFrameCount();
		}
	}
}

void MapGrid::GetUnits(UnitVector & units, BWAPI::Position center, int radius, bool ourUnits, bool oppUnits)
{
	const int x0(std::max( (center.x() - radius) / cellSize, 0));
	const int x1(std::min( (center.x() + radius) / cellSize, cols-1));
	const int y0(std::max( (center.y() - radius) / cellSize, 0));
	const int y1(std::min( (center.y() + radius) / cellSize, rows-1));
	const int radiusSq(radius * radius);
	for(int y(y0); y<=y1; ++y)
	{
		for(int x(x0); x<=x1; ++x)
		{
			int row = y;
			int col = x;

			GridCell & cell(getCellByIndex(row,col));
			if(ourUnits)
			{
				BOOST_FOREACH(BWAPI::Unit * unit, cell.ourUnits)
				{
					BWAPI::Position d(unit->getPosition() - center);
					if(d.x() * d.x() + d.y() * d.y() <= radiusSq)
					{
						if (!contains(units, unit)) 
						{
							units.push_back(unit);
						}
					}
				}
			}
			if(oppUnits)
			{
				BOOST_FOREACH(BWAPI::Unit * unit, cell.oppUnits) if (unit->getType() != BWAPI::UnitTypes::Unknown && unit->isVisible())
				{
					BWAPI::Position d(unit->getPosition() - center);
					if(d.x() * d.x() + d.y() * d.y() <= radiusSq)
					{
						if (!contains(units, unit)) 
						{ 
							units.push_back(unit); 
						}
					}
				}
			}
		}
	}
}

bool MapGrid::contains(UnitVector & units, BWAPI::Unit * unit) 
{
	for (size_t i(0); i<units.size(); ++i) 
	{
		if (units[i] == unit) 
		{
			return true;
		}
	}

	return false;
}

// Creates static danger map over cells
// Dangerous cells get their danger level set here
// Does not recalculate the map after first successful run
void MapGrid::CalculateDangerMap()
{
	if (dangerLevelCalculated)
		return;

	BWTA::BaseLocation * myBase = InformationManager::Instance().getMainBaseLocation(BWAPI::Broodwar->self());
	BWTA::BaseLocation * enemyBase = InformationManager::Instance().getMainBaseLocation(BWAPI::Broodwar->enemy());

	if (!myBase || !enemyBase)
		return;

	const BWTA::Polygon &enemyPoly = enemyBase->getRegion()->getPolygon();
	const BWTA::Polygon &myPoly = myBase->getRegion()->getPolygon();


	/// BASE DEFENSE ///
	set<BaseDefenseCell> baseDefenseCells;
	for (BWTA::Polygon::const_iterator i = enemyPoly.begin(); i != enemyPoly.end(); ++i)
		baseDefenseCells.insert(BaseDefenseCell(&getCell(BWAPI::Position(i->x(), i->y())), myBase->getPosition()));

	// Iterate through the closer half of the cells and set appropriate danger
	size_t cellNumber(0);
	for (set<BaseDefenseCell>::const_iterator i = baseDefenseCells.begin(); cellNumber < baseDefenseCells.size() / 2; ++i, ++cellNumber)
	{
		i->cell->danger = GridCell::BaseDefense;
		SetAdjecentTo(i->cell, GridCell::NearDanger);
	}


	/// WAR PATH ///
	// Shortest path base to base is the path units will most likely go to attack us
	vector<BWAPI::TilePosition> warPath = BWTA::getShortestPath(myBase->getTilePosition(), enemyBase->getTilePosition());
	list<GridCell*> warPathCells;
	// All cells war path goes through are considered dangerous
	for (vector<BWAPI::TilePosition>::const_iterator i = warPath.begin(); i != warPath.end(); ++i)
		warPathCells.push_back(&getCell(BWAPI::Position(i->x() * TILE_SIZE, i->y() * TILE_SIZE)));
	
	// Remove duplicates - no need to sort, since duplicates are always in clusters
	warPathCells.unique();

	// WP tiles closer to our base than this are ignored
	double warPathThreshold = myBase->getAirDistance(enemyBase) / 2;
	double wpt = myBase->getPosition().getDistance(enemyBase->getPosition()) / 2;
	for (list<GridCell*>::const_iterator i = warPathCells.begin(); i != warPathCells.end(); ++i)
	{
		// Cells close to our base and cells in enemy base polygon are not part of war path
		double distance = myBase->getPosition().getDistance((*i)->center);
		if (myBase->getPosition().getDistance((*i)->center) < warPathThreshold ||  myPoly.isInside((*i)->center) || enemyPoly.isInside((*i)->center))
			continue;
		(*i)->danger = GridCell::WarPath;
		
		// Also set danger for cells around war path
		SetAdjecentTo(*i, GridCell::AroundWarPath);
	}
	

	/// CHOKE POINTS ///
	const set<BWTA::Chokepoint*> &enemyChokes = enemyBase->getRegion()->getChokepoints();
	// Choke points are one of the most dangerous places in enemy base
	// Take all choke points and set danger level for them and adjacent tiles
	for (set<BWTA::Chokepoint*>::const_iterator i = enemyChokes.begin(); i != enemyChokes.end(); ++i)
	{
		GridCell &c = getCell(BWAPI::Position((*i)->getCenter().x(), (*i)->getCenter().y()));
		c.danger = GridCell::ChokePoint;
		
		// There may be turrets and whole army around the choke point
		SetAdjecentTo(&c, GridCell::ChokePoint);
	}

	dangerLevelCalculated = true;
	return;
}

void MapGrid::SetAdjecentTo(GridCell* cell, GridCell::DangerLevel danger)
{
	vector<GridCell*> adjacent = GetAdjacentCells(*cell);
	for (vector<GridCell*>::const_iterator adj = adjacent.begin(); adj != adjacent.end(); ++adj)
	{
		if ((*adj)->danger < danger)
			(*adj)->danger = danger;
		if (danger > GridCell::NearDanger)
			SetAdjecentTo(*adj, GridCell::NearDanger);
	}
}

const vector<GridCell*> MapGrid::GetAdjacentCells(const GridCell& cell)
{
	const CellCoords base = GetCellCoords(cell);
	vector<GridCell*> adjacent;

	for (int i = -1; i <= 1; ++i)
	{
		for (int j = -1; j <= 1; ++j)
		{
			// Base cell is not adjacent to itself
			if (i == 0 && j == 0)
				continue;

			int r = base.row + i;
			int c = base.col + j;

			// Check if given position is valid (on the map) and add it
			if (r >= 0 && c >= 0 && r < rows && c < cols)
				adjacent.push_back(&getCellByIndex(r, c));
		}
	}

	return adjacent;
}

/////////////////
/// PATH CELL ///
/////////////////
vector<PathCell> PathCell::GetAdjacent(const BWAPI::Position &enemyBase) const
{
	vector<PathCell> adjacentPathCells;
	vector<GridCell*> adjacent = MapGrid::Instance().GetAdjacentCells(*this->cell);
	for (vector<GridCell*>::const_iterator i = adjacent.begin(); i != adjacent.end(); ++i)
	{
		adjacentPathCells.push_back(PathCell(*i, enemyBase));
	}

	return adjacentPathCells;
}


/////////////////
/// GRID CELL ///
/////////////////
string GridCell::DangerToString() const
{
	switch (danger)
	{
	case GridCell::Safe:
		return "Safe";
	case GridCell::NearDanger:
		return "Near Danger";
	case GridCell::AroundWarPath:
		return "Around War Path";
	case GridCell::BaseDefense:
		return "Base Defense";
	case GridCell::WarPath:
		return "War Path";
	case GridCell::ChokePoint:
		return "Choke Point";
	default:
		return "Unknown";
	}
}

BWAPI::Color GridCell::GetColor() const
{
	switch (danger)
	{
	case GridCell::Safe:
		return BWAPI::Colors::Green;
	case GridCell::NearDanger:
		return BWAPI::Colors::Cyan;
	case GridCell::AroundWarPath:
		return BWAPI::Colors::Orange;
	case GridCell::BaseDefense:
		return BWAPI::Colors::Blue;
	case GridCell::WarPath:
		return BWAPI::Colors::Red;
	case GridCell::ChokePoint:
		return BWAPI::Colors::Yellow;
	default:
		return BWAPI::Colors::Black;
	}
}

int GridCell::GetCost() const
{
	switch (danger)
	{
	case GridCell::Safe: // Safe is the best way to go
		return 1;
	case GridCell::NearDanger: // Tiles adjacent to dangerous tiles are virtually one more tile away form the goal
		return 2;
	case GridCell::AroundWarPath: // Dangerous
		return 1000;
	case GridCell::BaseDefense: // If there is no safe way into base - this is our best bet
		return 500;
	case GridCell::WarPath: // Dangerous
		return 1000;
	case GridCell::ChokePoint: // Super dangerous
		return 10000;
	default:
		return 10000;
	}
}